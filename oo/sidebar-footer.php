<!--底部介绍及版权-->
            <footer class="footerWra">
                <div class="footer">
                    <article>
                        <p class="topInfoAll short">
                            *戴尔的常规条款和条件在此适用，并可网上获得或致函索取。戴尔会尽力排查报价错误或其他错误，但由于我们的疏漏，某些错误仍可能发生。戴尔有权利不接受任何包含错误信息的订单。以上图片仅供参考。戴尔、戴尔标志、the
                            stylized E logo, E-Value, Easy as Dell, Dimension, OptiPlex, Inspiron, Latitude, Dell
                            Precision, PowerEdge, PowerVault, PowerConnect, Dell OpenManage, CompleteCover, Premier
                            Access, DellNet, SmartStep, TrueMobile, Axim是Dell Inc.的注册商标或商标。Ultrabook、赛扬、Celeron
                            Inside、Core Inside、英特尔、英特尔标志、英特尔凌动、Intel Atom Inside、英特尔酷睿、Intel Inside、Intel Inside
                            标志、英特尔博锐、安腾、Itanium Inside、奔腾、Pentium Inside、vPro Inside、至强、至强融核、Xeon Inside
                            和英特尔傲腾是英特尔公司或其子公司在美国和/或其他国家（地区）的商标。微软、微软标识和Windows是微软公司在美国和其他国家的商标/注册商标。文中提及的其他商标或商品名称均指拥有该商标或名称的机构或其产品。戴尔不拥有其他机构的商标和商品名称的相关权益。
                            “Dell® / EMC ® / Dell EMC ® “ 等品牌商标将有可能同时出现在戴尔易安信相关企业级产品 (包括硬件和软件)， 和/或产品资料、戴尔易安信的官方网站。
                            如果您有有关戴尔易安信产品相关的任何疑问，欢迎联系您的指定客户经理。
                        </p>
                        <div class="footJtD">
                            <span class="icon iconfont icon-xiangxia2" style="color: #bbb; font-size: 16px;"></span>
                            <span class="icon iconfont icon-xiangshang2"
                                style="color: #bbb; font-size: 16px; display: none;"></span>
                        </div>
                    </article>

                    <p class="bottomInfo">
                        <span>*戴尔仅在符合相关法律法规及<a href="https://www.dell.com/learn/cn/zh/cncorp1/policies-privacy"
                                target="_blank"
                                class="color-dell">隐私声明</a>下收集与使用客户访问信息。戴尔（中国）有限公司，电话：86-0592-8181888。</span>
                        <span class="mar-t-10">请浏览以下网站获得更多信息 <a
                                href="https://www.dell.com/learn/cn/zh/cncorp1/solutions/art-important-dell-detail-cn?c=cn&amp;l=zh&amp;s=corp&amp;redirect=1"
                                target="_blank" class="color-dell">www.dell.com.cn/product_information</a></span>
                        <span>版权：©2018 Dell Inc. 版权所有</span>
                        <span class="Copyright"><img src="<?php bloginfo('template_directory'); ?>/img/guohui.png" class="guohui"><a
                                href="http://beian.miit.gov.cn/" target="_blank"
                                class="beian">京ICP备11038982号-18</a></span>
                    </p>
                </div>
            </footer>