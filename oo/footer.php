
    <!--公共js引用-->
    <script src="<?php bloginfo('template_directory'); ?>/img/jquery-1.11.2.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/jquery.cookie.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/imagesloaded.pkgd.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/infinite-scroll.pkgd.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/masonry.pkgd.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/config.js"></script>
    <!--smarket引用-->
    <script src="<?php bloginfo('template_directory'); ?>/img/knockout.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/knockout.validation.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/md5.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/smarket.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/app.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/dialog.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/userRecode.js"></script>
    <!-- <script src="<?php bloginfo('template_directory'); ?>/img/masonry.js"></script> -->
    <script src="<?php bloginfo('template_directory'); ?>/img/tab.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/filelist.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/home.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/navFoot.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/popUp.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/index.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/init.js"></script>
    <!--页面js引用-->

    <script src="<?php bloginfo('template_directory'); ?>/img/swiper.min.js"></script>
    <script>
        $(function(){
            //banner轮播位置必须在显示之后
            var swiper = new Swiper('.swiper-container', {
                spaceBetween: 0,
                centeredSlides: true,
                // autoplay:false,
                autoplay: {
                    delay: 3000,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });


            setTimeout(() => {
                $(".load").hide();
                $('.content-all').masonry({
                columnWidth: 0,
                itemSelector: '.content-item'
                });
            },2000);
            

            
            $('.meCore').hover(function(){
                $('.meCoreList').stop();
                $('.meCoreList').fadeIn(400);
            },function(){
                $('.meCoreList').stop();
                $('.meCoreList').fadeOut(400);
                $('.meCore').css({
                    'background-color': 'rgba(0,0,0,0)'
                })
            });

        })
    </script>


    <script src="<?php bloginfo('template_directory'); ?>/img/sort.js"></script>

</body>
</html>