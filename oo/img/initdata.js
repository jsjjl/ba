//调试环境设置
window.isDebug = false;

//基础url配置
window.urlConfig = {
    //url通过后台传来的rootUrl设定
    type:'//',
    apiBaseUrl:'www.dellemc-solution.com',
    baseUrl:'www.dellemc-solution.com',
    host:window.location.host,

    //模块url
    modulePath:window.location.pathname.split('/')[1],

    //老站接口和网址写死这里
    oldApiUrl:'api.dellemc-solution.com',
    oldBaseUrl:'www.dellemc-solution.com',

    backUrl: encodeURIComponent(window.location.href),
};

//处理modulePath ex: 'path/' or ''
if(window.urlConfig.modulePath === '/' || window.urlConfig.modulePath === '' || window.urlConfig.modulePath.indexOf('.html') > -1){
    window.urlConfig.modulePath = '';
}else{
    window.urlConfig.modulePath += '/';
}

//推荐页原首页特殊处理
if(window.urlConfig.modulePath === 'recommend/'){
    window.urlConfig.modulePath = '';
}

//后端获取url地址
if(window.rootUrl){
    var urlIndex = window.rootUrl.indexOf('https://');
    if(urlIndex > -1 && !window.isDebug){
        window.urlConfig.type = 'https://';
        window.urlConfig.apiBaseUrl = window.urlConfig.baseUrl = window.rootUrl.substr(8);
    }else{
        window.urlConfig.type = 'http://';
        window.urlConfig.apiBaseUrl = window.urlConfig.baseUrl = window.rootUrl.substr(7);
    }
}

//初始化数据状态
window.cateData = {
    device:'pc',
    //smarket ID 配置
    weChatId: 34443,//正式服务号戴尔易安信市场活动
    tenantId: 444,
    schemaId: 498,
    rigisterFormId: 1386,
    rigisterVisitorFormId: 1387,

    //搜索type
    searchType: 'all',

    //各tab是否有数据
    isHaveData: {
        all: false,
        program: false,
        tank: false,
        activity: false,
        media: false,
        product: false,
        lightRead: false,
        evaluation: false
    },

    //各瀑布流无数据隐藏加载
    isLoadShow: {
        all: false,
        program: false,
        tank: false,
        activity: false,
        media: false,
        product: false,
        lightRead: false,
        evaluation: false
    },

    //定义modul
    pageModuleName: '行业',
    pageModule: '行业',

    //定义行业
    pageIndustryName: '教育',
    pageIndustryType: 'education',
    pageTab: 'all',

    //页面排序
    pageSort:'all',

    //tab页载入动态数据页面索引
    pageTabIndex: {
        all: 2,
        program: 2,
        tank: 2,
        activity: 2,
        media: 2,
        product: 2,
        lightRead: 2,
        evaluation: 2
    },

    //tab页是否全部加载完毕
    pageTabIsLoadEnd: {
        all: false,
        program: false,
        tank: false,
        activity: false,
        media: false,
        product: false,
        lightRead: false,
        evaluation: false
    },

    //无限加载实力
    infScroll: null,

    //接口中的数据量 小于8 为没有数据了
    msItemCount: {
        all: 8,
        program: 8,
        tank: 8,
        activity: 8,
        media: 8,
        product: 8,
        lightRead: 8,
        evaluation: 8
    },

    //用户行为记录接口地址
    userActionUrl: window.urlConfig.type + window.urlConfig.oldApiUrl + (window.isDebug ? '/uat/interaction/create' : '/api/interaction/create'),
    //内容加载接口地址
    getContentUrl: window.urlConfig.type + window.urlConfig.apiBaseUrl + '/api/industry/',

    //每页加载数量
    pageItemNum:8,

    firstLoad:false,

    //URL链接
    url:{
        login: window.urlConfig.type + window.urlConfig.baseUrl + '/personcenter/login.html',
        backUrl: encodeURIComponent(window.location.href),
        loginAndBack: window.urlConfig.type + window.urlConfig.baseUrl + '/personcenter/login.html?backUrl=' + window.urlConfig.backUrl,

        //案例智库
        page: window.urlConfig.type + window.urlConfig.host + '/' + window.urlConfig.modulePath,
    },
    //API
    api:{
        like: window.urlConfig.type + window.urlConfig.apiBaseUrl + '/api/interaction/collect',
        getItem: window.urlConfig.type + window.urlConfig.apiBaseUrl + '/api/' + window.urlConfig.modulePath,
        getItemCondition:'',
        // getCollectionStatus: window.urlConfig.type + window.urlConfig.apiBaseUrl + '/api/statInfo/get',
        getGlobalUserId: window.urlConfig.type + window.urlConfig.oldApiUrl + '/anonymous/getId',
        getUserInfo: window.urlConfig.type + window.urlConfig.oldApiUrl + '/member/LookUp',
        getUserId: window.urlConfig.type + window.urlConfig.oldApiUrl + '/api/member/contacts/getbindinfo',
        getCode: window.urlConfig.type+window.urlConfig.oldApiUrl+'/member/sendCheckCode',
        checkCode: window.urlConfig.type+window.urlConfig.oldApiUrl+'/member/checkCode',
        checkCodePwd:window.urlConfig.type+window.urlConfig.oldApiUrl+'/member/GetToken',
        getInitInfo:window.urlConfig.type+window.urlConfig.oldApiUrl+'/member/form/GetForNewForm',
        changePwd:window.urlConfig.type+window.urlConfig.oldApiUrl+'/member/changePwd',
        modifyInfo:window.urlConfig.type+window.urlConfig.oldApiUrl+'/member/UpdateIdentityInfo',

        //通过email下载文件
        downloadWithEmail: window.urlConfig.type + window.urlConfig.oldApiUrl + '/file/downloadWithEmail',
        downloadByEmail: window.urlConfig.type + window.urlConfig.oldApiUrl + '/project/dell/file/downloadbyemail',

        //删除评论
        delCommnet: window.urlConfig.type + window.urlConfig.apiBaseUrl + '/api/interaction/comment/delete',

        //获取表单
        getForm: 'https://api.dellemc-solution.com/customForm/get',

        //提交表单
        actionForm: 'https://api.dellemc-solution.com/customForm/action',

        //获取表单提交状态
        checkRegistration: 'https://api.dellemc-solution.com/customForm/checkRegistration',

        //获取字典表
        getDict: 'https://api.dellemc-solution.com/dic/params/getTree'
    },

    //批量议价单配置
    batchFrome: {
        formName:"批量议价单",
        formId:3496,
        customFilds:{
            productName:'custom_form_field_1557974747778',
            productId:'custom_form_field_1557974766159',
            productPrice:'custom_form_field_1557974773057',
            productNum:'custom_form_field_1557974782981'
        },

        behaviorType:"提交批量议价",
        gioType:'bulkPremiumClicksubmit'
    },

    //商城采购单配置
    purchaseForm: {
        formName:"商城采购单",
        formId:3862,
        customFilds:{
            productName:'custom_form_field_1559620467120',
            productId:'custom_form_field_1559620468664',
            productPrice:'custom_form_field_1559620469088',
            productNum:'custom_form_field_1559620469534'
        },

        behaviorType:"提交商城采购",
        gioType:'buysubmit_eshop'
    },

    //用户行为设置 用于接收后台传值
    userRecodeOption:{
        module: '',
        title: '',
        category1: '',
        tag:'',
        behaviorType:'浏览网页',
        behaviorResult:'浏览成功',
    },

    //用户sess
    userInfo:{
        sess:'',
        memberId:'',
    },

    //多少秒后弹出客服s
    kefuOpenTime:40,

    //发送文件配置
    email: {
        emailSubject: '戴尔易安信(Dell EMC)解决方案中心-相关资料下载',
        senderEmail: 'dellEmc@dell.com',
        senderName: '戴尔易安信(Dell EMC)解决方案中心'
    },
};