var sortBox = {
    //过滤条件
    filterArr: [],
    //过滤url
    filterUrl: "",
    //过滤窗显示状态
    isShowFilterBox:false,
    //基础url
    filterBaseUrl:'',
    //排序类型
    sort:'all',
    sortUrl:'',
    //请求文章的api tagsurl
    apiTagsUrl:'',
    subCateArr:[], //子分类的数组
    apiSubCateUrl:'', //子分类的URL

    //注册点击事件
    regFilterEvent: function () {
        var _this = this;

        //点击筛选条件
        $('.conditionItem>span').on('click', function () {
            _this.setFilter(this);
        });

        //点击完成
        $('.conditionsBtn .btnFinish').on('click', function () {
            _this.done(this);
        });

        //点击清空条件
        $('.conditionsBtn .btnClear').on('click', function () {
            _this.clear(this);
        });

        //点击清空跳转页面
        $('.resultBtn .btnRClear').on('click', function () {
            _this.setSortUrl();
            window.location.href = _this.filterBaseUrl + _this.sortUrl;
        });

        //点击sortTab
        $('.sort-tab-item').on('click', function () {
            // var filterTabName = $(this).attr('bt-data');
            // _this.sort = filterTabName;
            // _this.setSortUrl();
            // _this.setFilterUrl();
            // //跳转
            // window.location.href = _this.filterBaseUrl + _this.filterUrl + _this.sortUrl;
        });

        //点击叉号取消筛选
        $('.resultItem li').on('click', function () {
            $(this).remove();
            _this.setFilterUrl('delete');
            _this.setSortUrl();
            window.location.href = _this.filterBaseUrl + _this.filterUrl + _this.sortUrl;
        });

        //点击筛选tab
        $('.sort-filter-item,.btnOpen').on('click', function () {
            if(_this.isShowFilterBox){
                $(this).removeClass('ck');
                _this.showFilterBox('hide');
            }else{
                //判断是否是点击选中
                $('.conditionItem span').each(function(){
                    if($(this).attr('data-status') == 'on'){
                        $(this).addClass('filter-seled');
                    }else{
                        $(this).removeClass('filter-seled');
                    }
                });
                // $(this).addClass('ck');
                _this.showFilterBox('show');
            }
        });
    },

    //设置展开栏的高度
    setHeight: function () {
        var h = window.innerHeight * 0.5;
        $('.filter-content').height(h);
    },

    setFilter: function (elem) {
        $(elem).toggleClass('filter-seled');
    },

    //完成筛选
    done: function () {
        var _this = this;
        _this.showFilterBox('hide');
        _this.setFilterUrl();
        var url = window.location.pathname;
        var index = url.lastIndexOf("\/");
        var str = url.substring(index + 1,url.length);
        //如果当前的筛选项没有变更则不跳转
        if(str !== _this.filterUrl){
            window.location.href = _this.filterBaseUrl + _this.filterUrl + _this.sortUrl;
        }
    },

    //设置过滤器url
    setFilterUrl: function (type) {
        var _this = this;
        //tags ID 数组
        _this.filterArr = [];
        _this.apiTagsUrl = [];
        _this.subCateArr = [];

        //获取已选中/删除后的tag标签ID
        var filterFrom = '.conditionItem';
        if (type == 'delete') {
            filterFrom = '.resultItem';
        }

        $(filterFrom + ' .filter-seled').each(function (index, elem) {
            var tagId = elem.getAttribute('filter-id');
            //解决方案中的类型为子分类需要进行特殊处理
            if (parseInt(tagId)) {
                _this.filterArr.push(tagId);
            } else {
                _this.subCateArr.push(tagId);
            }
        });

        //排序数组
        _this.filterArr.sort(sortNum);
        function sortNum(a, b) {
            return a - b;
        }

        //处理子分类筛选条件url exp: it_scl
        _this.apiSubCateUrl =  '';
        if(_this.subCateArr.length > 0){
            _this.subCateArr.forEach(function (item, index) {
                if(index === 0){
                    _this.apiSubCateUrl = item;
                }else{
                    _this.apiSubCateUrl += '_' + item;
                }
            });
        }else{
            _this.apiSubCateUrl = '0'; //如果没有选择分类则置为0
        }

        //开始拼接url
        _this.filterUrl = ''; //用于跳转链接

        //part1
        if(_this.filterArr.length>0){ //如果tagsArr不为空
            if(window.cateData.pageModule === 'program' || window.cateData.pageModule === 'filelist'){ //解决方案/资料中心拼接分类
                _this.filterUrl = 'index-'+ _this.apiSubCateUrl + '-';
            }else{
                _this.filterUrl = 'index-';
            }
        }else{ //如果tagsArr为空则结束拼接
            if(_this.apiSubCateUrl === '0'){ //分类url为0 则不拼接到url
                _this.filterUrl = '';
            }else{
                _this.filterUrl = 'index-'+ _this.apiSubCateUrl + '.html';
            }
            return;
        }

        //part2 处理tags
        _this.filterArr.forEach(function (item, index) {
            if(index === 0){
                _this.filterUrl += item;
                _this.apiTagsUrl += '-' + item; //用于列表请求接口
            }else{
                _this.filterUrl += '_' + item;
                _this.apiTagsUrl += '_' + item; //用于列表请求接口
            }
        });

        //part3
        _this.filterUrl = _this.filterUrl + '.html';
    },

    //设置瀑布流获取文章接口的条件部分
    setCnditionUrl:function(){
        if(window.cateData.pageModule === 'tank' || window.cateData.pageModule === 'evaluation'){ //如果是智库则不添加子分类条件
            window.cateData.api.getItemCondition = window.cateData.pageSort + this.apiTagsUrl;
        }else{
            window.cateData.api.getItemCondition = this.apiSubCateUrl + '/'+window.cateData.pageSort + this.apiTagsUrl;
        }
    },

    clear: function () {
        $('.filter-seled').removeClass('filter-seled');
        this.filterArr = [];
    },

    //控制筛选窗的显隐
    showFilterBox: function (display) {
        var _this = this;
        if (display === 'show') {
            _this.isShowFilterBox = true;
            $('.conditionsWra').removeClass('hide');
        } else {
            $('.conditionsWra').addClass('ani-to-top-filter-box');
            $('.conditionsWra').removeClass('ani-form-top-filter-box');
            setTimeout(function () {
                _this.isShowFilterBox = false;
                $('.conditionsWra').addClass('hide');
                $('.conditionsWra').removeClass('ani-to-top-filter-box');
                $('.conditionsWra').addClass('ani-form-top-filter-box');
            }, 400);
        }
    },

    setOption:function(){
        this.filterBaseUrl = window.cateData.url.page;
        this.sort = window.cateData.pageSort;
        //当前sortTab
        this.setSortUrl();
    },

    setSortUrl:function(){
        if(this.sort === 'all'){
            this.sortUrl = '';
        }else{
            this.sortUrl = '?sort=' + this.sort;
        }
    },

    setFilterTabSeld: function(){
        if(this.filterArr.length >0 || this.subCateArr.length > 0){
            $('.sort-filter-item').addClass('seled');
        }
    },

    init: function () {
        this.setOption();
        this.regFilterEvent();
        this.setHeight();
        this.setFilterUrl();
        this.setCnditionUrl();
        this.setFilterTabSeld();
    },
};
sortBox.init();