var msnObj = {
  createMsn: function () {
    //获取需要处理的容器
    this.gridAll = document.querySelector('.content-all');
    this.gridProgram = document.querySelector('.content-program');
    this.gridTank = document.querySelector('.content-tank');
    this.gridActivity = document.querySelector('.content-activity');
    this.gridLightRead = document.querySelector('.content-lightRead');
    this.gridEvaluation = document.querySelector('.content-evaluation');

    //瀑布流配置
    var masOption = {
      itemSelector: 'none', // select none at first
      // percentPosition: true,
      stagger: 30, //错开
      transitionDuration: 0 //动画时间
      // columnWidth: '.grid__col-sizer',//元素宽度
      // gutter: '.grid__gutter-sizer', //间距宽度
      // nicer reveal transition
      // visibleStyle: {transform: 'translateY(0)', opacity: 1},
      // hiddenStyle: {transform: 'translateY(100px)', opacity: 0},
    };

    //创建瀑布流 如果没有数据就不创建
    if(window.cateData.isHaveData.all){
      this.msnryArr = new Masonry(this.gridAll, masOption);
      this.loadPage(this.gridAll, this.msnryArr);
    }else if(window.cateData.isLoadShow.all){
      $('.load').hide();
    }
    if(window.cateData.isHaveData.program){
      this.msnryProgram = new Masonry(this.gridProgram, masOption);
      this.loadPage(this.gridProgram, this.msnryProgram);
    }
    if(window.cateData.isHaveData.tank){
      this.msnryTank = new Masonry(this.gridTank, masOption);
      this.loadPage(this.gridTank, this.msnryTank);
    }
    if(window.cateData.isHaveData.activity){
      this.msnryActivity = new Masonry(this.gridActivity, masOption);
      this.loadPage(this.gridActivity, this.msnryActivity);
    }
    if(window.cateData.isHaveData.lightRead){
      this.msnryLightRead = new Masonry(this.gridLightRead, masOption);
      this.loadPage(this.gridLightRead, this.msnryLightRead);
    }
    if(window.cateData.isHaveData.evaluation){
      this.msnryEvaluation = new Masonry(this.gridEvaluation, masOption);
      this.loadPage(this.gridEvaluation, this.msnryEvaluation);
    }
  },

  //进入页面重载布局
  loadPage: function (grid, msnry) {
    // 图片载入完成后再加载瀑布流样式
    imagesLoaded(grid, function () {
      msnry.options.itemSelector = '.content-item';
      var items = grid.querySelectorAll('.content-item');
      msnry.appended(items);
      msnry.layout();
      if(!window.cateData.firstLoad){
        window.cateData.firstLoad = true;
        setTimeout(function () {
          $('.load').hide();
        },1000);
      }
    });
  },

  //初始化无限滚动 瀑布流容器class sol行业 type 分类
  initInfScroll: function (grid, msnry, type) {
    var _this = this;
    if (window.cateData.msItemCount[window.cateData.pageTab] < window.cateData.pageItemNum) {
      return;
    }
    //初始化无限滚动
    window.cateData.infScroll = new InfiniteScroll(grid, {
      path: function () {
        // 如果上一次接口中的数据不足8条则不再加载 当正在加载的时候不请求下一次的数据
        if (window.cateData.msItemCount[window.cateData.pageTab] >= window.cateData.pageItemNum) {
          //baseUrl/栏目/子栏目(可选)/条件/pc/分页
          //根据模块切换接口地址
          if(window.cateData.pageModule === 'industry'){
            return window.cateData.getContentUrl + window.cateData.pageIndustryType +'/' + type + '/pc/' + window.cateData.pageTabIndex[window.cateData.pageTab];
          }else if(window.cateData.pageModule === 'index'){
            return window.cateData.api.getItem +  window.cateData.api.getItemCondition +'/' + type + '/'+ window.cateData.device +'/' + window.cateData.pageTabIndex[window.cateData.pageTab];
          }else{
            return window.cateData.api.getItem +  window.cateData.api.getItemCondition +'/' +  window.cateData.device +'/' + window.cateData.pageTabIndex[window.cateData.pageTab];
          }
        }
      },
      checkLastPage: true,
      responseType: 'text',
      outlayer: msnry,
      // status: '.loading-box', //加载状态
      history: false,//写入浏览器历史记录
    });

    var proxyElem = document.createElement('div');

    //监听
    window.cateData.infScroll.on('load', function (response) {
      //处理数据中禁止再次加载
      window.cateData.infScroll.canLoad = false;
      var data = JSON.parse(response);
      //设置本次获取的数据条数
      window.cateData.msItemCount[window.cateData.pageTab] = data.count;
      //如果本次获取的数据小于8 则为最后一页 设置变量
      if(data.count < window.cateData.pageItemNum){
        window.cateData.pageTabIsLoadEnd[window.cateData.pageTab] = true;
      }
      //页面索引加1
      window.cateData.pageTabIndex[window.cateData.pageTab] += 1;

      var itemsHTML = data.content.map(_this.getItemHTML).join('');
      proxyElem.innerHTML = itemsHTML;
      var items = proxyElem.querySelectorAll('.content-item');

      //这里把实例赋给一个变量，避免切换tab后数据插入错误
      var infScrollObj = window.cateData.infScroll;
      imagesLoaded(items, function () {

        infScrollObj.appendItems(items);
        msnry.appended(items);

        //数据处理完毕，打开滚动加载
        window.cateData.infScroll.canLoad = true;

        //再初始化一下标签事件，要不新增的标签不能点击 是跳链接不是切tab注释掉
        // pageTab.init();
        stopRurnLik();

        if (window.cateData.msItemCount[window.cateData.pageTab] < window.cateData.pageItemNum) {
          //将loader设置为加载完成
          msLoader.setEnd(window.cateData.pageTab);

          //如果没有下一页就销毁
          window.cateData.infScroll.destroy();
        } else {
          msLoader.setLoad(window.cateData.pageTab);
        }
      });
    });

    //如果load出错
    window.cateData.infScroll.on('error', function () {
      // msLoader.setEnd();
    });

    //触发滚动
    window.cateData.infScroll.on( 'scrollThreshold', function() {
      // msLoader.setShow();
    });
    //发出请求时触发
    window.cateData.infScroll.on( 'request', function( path ) {
    });

    // load initial page
    // infScroll.loadNextPage();
  },

  //切换tab后需要重载定位样式
  layout: function (cate) {
    //销毁infScroll
    if(window.cateData.infScroll){
      window.cateData.infScroll.destroy();
    }

    if (cate === 'all'  && window.cateData.isHaveData[cate]) {
      this.msnryArr.layout();
      this.initInfScroll(this.gridAll, this.msnryArr, 'all');
    }
    if (cate === 'program' && window.cateData.isHaveData[cate]) {
      this.msnryProgram.layout();
      this.initInfScroll(this.gridProgram, this.msnryProgram, 'solution');
    }
    if (cate === 'tank' && window.cateData.isHaveData[cate]) {
      this.msnryTank.layout();
      this.initInfScroll(this.gridTank, this.msnryTank, 'case');
    }
    if (cate === 'activity' && window.cateData.isHaveData[cate]) {
      this.msnryActivity.layout();
      this.initInfScroll(this.gridActivity, this.msnryActivity, 'market');
    }
    //新增轻阅读
    if (cate === 'lightRead' && window.cateData.isHaveData[cate]) {
      this.msnryLightRead.layout();
      this.initInfScroll(this.gridLightRead, this.msnryLightRead, 'reading');
    }
    if (cate === 'evaluation' && window.cateData.isHaveData[cate]) {
      this.msnryEvaluation.layout();
      this.initInfScroll(this.gridEvaluation, this.msnryEvaluation, 'evaluation');
    }
    if (cate === 'media' && window.cateData.isHaveData[cate]) {
      this.msnryMedia.layout();
      this.initInfScroll(this.gridMedia, this.msnryMedia, 'media');
    }
    if (cate === 'product' && window.cateData.isHaveData[cate]) {
      this.msnryProduct.layout();
      this.initInfScroll(this.gridProduct, this.msnryProduct, 'product');
    }
    //增加轻阅读模块请求

    //如果没有数据隐藏loading
    if(!window.cateData.isHaveData[cate]){
      msLoader.setHide();
    }else{
      msLoader.setShow();
    }

    //如果已经加载完成则loader显示加载完成提示 或 当前一页的数据小于设定数据
    if(window.cateData.pageTabIsLoadEnd[window.cateData.pageTab] || window.cateData.msItemCount[window.cateData.pageTab] < window.cateData.pageItemNum){
      msLoader.setEnd();
    }
  },

  //获取模板
  getItemHTML:function(photo){
    var itemTemplateSrc = document.querySelector('#photo-item-template').innerHTML;
    return msnObj.microTemplate(itemTemplateSrc, photo);
  },

  //渲染模板
  microTemplate:function(src, data){
    //是否有描述
    if (data.summary && (data.type === 'activity' || data.type === 'product' || window.cateData.pageIndustryType === 'tank' || window.cateData.pageIndustryType === 'program' || window.cateData.pageIndustryType === 'filelist' || window.cateData.pageIndustryType === 'evaluation')) {
      data.showdesc = '';
      // console.log(data.summary.length);
      if(data.summary.length > 73){
        data.summaryShow = (data.summary).substr(0,73)+'...';
      }else {
        data.summaryShow = data.summary;
        //不显示标题置空
        data.summary = '';
      }
    }else{
      data.showdesc = 'hideIm';
    }

    //置顶图标
    if (data.recommand == 1){
      data.topShow = '';
    }else{
      data.topShow = 'hide';
    }

    //资料中心下载蒙层
    if (data.type === 'filelist' && data.fileType === 'file'){
      data.showFile = '';
    }else{
      data.showFile = 'hide';
    }

    //产品中心添加价格显示
    if (data.type === 'product'){
      data.showPriceWra = '';
      if(data.price){
        if(data.price == '联系戴尔'){
          data.showPrice = 'hideIm';
          data.showService = '';
        }else{
          data.showPrice = '';
          data.showService = 'hideIm';
        }
      }else{
        data.showPrice = 'hideIm';
        data.showService = '';
      }
    }else{
      data.showPriceWra = 'hideIm';
    }

    //是否有链接
    if (!data.url) {
      data.itemurl = 'javascript:void(0);';
    }else{
      data.itemurl = data.url;
    }

    //如果是智库/解决方案/资料中心模块则不显示类型标签和发布时间
    if(window.cateData.pageIndustryType === 'tank' || window.cateData.pageIndustryType === 'program' || window.cateData.pageIndustryType === 'filelist'  || window.cateData.pageIndustryType === 'evaluation'){
      data.showAuthor = '';
      //区分显示作者还是文件大小
      if(window.cateData.pageIndustryType === 'filelist'){
        if(data.fileType == 'file'){
          //文件显示文件大小
          if (data.fileSize) {
            data.showTimeInfo = '';
            data.showFileType = '';
            data.showAuthorType = 'hide';
            data.timeInfo = data.fileSize + '(MB)';
          }else{
            data.showTimeInfo = 'hideIm';
            data.timeInfo = '';
          }
          data.showGf = 'hideIm';
        }else{
          //视频显示作者
          if (data.author) {
            data.showTimeInfo = '';
            data.timeInfo = data.author;
            data.showFileType = 'hide';
            data.showAuthorType = '';
            if(data.author_vip == 1){
              data.showGf = '';
            }else{
              data.showGf = 'hideIm';
            }
          }else{
            data.showTimeInfo = 'hideIm';
            data.timeInfo = '';
            data.showGf = 'hideIm';
          }
        }
      }else{
        //案例智库/解决方案显示作者
        if (data.author) {
          data.showTimeInfo = '';
          data.showFileType = 'hide';
          data.showAuthorType = '';
          data.timeInfo = data.author;
          if(data.author_vip == 1){
            data.showGf = '';
          }else{
            data.showGf = 'hideIm';
          }
        }else{
          data.showTimeInfo = 'hideIm';
          data.timeInfo = '';
          data.showGf = 'hideIm';
        }
      }
      data.showTypeRow = 'hide';
      data.showTimeRow = '';
      data.showTypePad = 'pad-t-0';
    }else{
      if(data.fileType == 'file'){
        //文件显示文件大小
        if (data.fileSize) {
          data.showTimeInfo = '';
          data.showFileType = '';
          data.showAuthorType = 'hide';
          data.timeInfo = data.fileSize + '(MB)';
        }else{
          data.showTimeInfo = 'hideIm';
          data.timeInfo = '';
        }
        data.showGf = 'hideIm';
        data.showTimeRow = '';
        data.showAuthor = 'hideIm';
      }else{
        if (data.author && data.author_vip == 1) {
          data.showGf = '';
        }else{
          data.showGf = 'hideIm';
        }
        data.showTimeRow = 'hide';
        data.showAuthor = '';
        data.showFileType = 'hide';
        data.showAuthorType = '';
      }
      data.showTypeRow = '';
      data.showTypePad = '';
    }

    //没有图片，替换默认图片
    if (!data.coverurl) {
      if(data.status == 2){
        data.coverurl = '/static/pc/img/newStyle/defaultPlay.jpg';
      }else{
        data.coverurl = '/static/pc/img/newStyle/default.jpg';
      }
    }

    //高度差异化
    if(data.type != 'activity'){
      if (data.randStyle == 0) {
        data.differH = '';
      }else{
        data.differH = 'maxH-448';
      }
    }else{
      data.differH = '';
    }

    //判断是否直播中显示
    if(data.status == 2){
      data.normalRemove = 'hide';
      data.marketRemove = '';
      data.coverClass = 'imgMitem';
      //直播中标题限制两行显示
      if(data.title.length > 35){
        data.titleShow = (data.title).substr(0,35)+'...';
      }else {
        data.titleShow = data.title;
        //不显示标题置空
        data.title = '';
      }
    }else{
      data.normalRemove = '';
      data.marketRemove = 'hide';
      data.coverClass = 'imgItem';
      data.titleShow = data.title;
      //不显示标题置空
      data.title = '';
    }

    //判断是否显示评论/点赞/作者
    if(data.type == 'product' || data.type == 'activity'){
      data.showComment = 'hide';
    }else {
      data.showComment = '';
    }

    //判断直播状态
    if(data.type == 'activity'){
      if(data.status == 3){
        data.marketStatus = 'sFinish';
        data.showPlay = '';
      }else{
        data.marketStatus = 'sSign';
        data.showPlay = 'hide';
      }
      data.doPlay = 'hideIm';
      data.listStyle = 'normalList';
    }else {
      if(data.type == 'filelist' && data.fileType == 'video'){
        data.showPlay = 'hide';
        data.doPlay = '';
        data.listStyle = 'videoList';
      }else{
        data.showPlay = 'hide';
        data.doPlay = 'hideIm';
        data.listStyle = 'normalList';
      }
      data.marketStatus = 'hide';
    }

    //点赞状态
    if(data.collect === 0){
      data.collectType = 'false';
    }else{
      data.collectType = 'true';
    }

    // 替换{{}}标签资源
    return src.replace(/\{\{([\w\-_\.]+)\}\}/gi, function (match, key) {
      var value = data;
      key.split('.').forEach(function (part) {
        value = value[part];
      });
      return value;
    })
  }
};

//控制loading
var msLoader = {
  setEnd: function (type) {
    setTimeout(function () {
      $('.loading-infinite').hide();
      $('.loading-end').show();
      $('.normalRWra').each(function(){
        if($(this).hasClass('hide')){
          $(this).remove();
        }
      });
      $('.marketRWra').each(function(){
        if($(this).hasClass('hide')){
          $(this).remove();
        }
      });
    }, 1000);
  },
  setLoad: function (type) {
    $('.loading-infinite').show();
    $('.loading-end').hide();
  },
  setHide: function(){
    $('.loading-box').hide();
  },
  setShow: function(){
    $('.loading-box').show();
    this.setLoad();
  }
};