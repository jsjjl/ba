var pageInit = function(){
    // msLoader.setShow();
    $('.contentWrap').fadeIn(400);
    // $('.load').hide();

    //获取用户sess
    var userInfo = isLogin();
    window.cateData.userInfo.sess = userInfo?userInfo.session:'';
    window.cateData.userInfo.memberId = userInfo?userInfo.memberId:'';

    //创建瀑布流
    // msnObj.createMsn();
    //启用全部tab
    // msnObj.layout('all');

    //初始化用户行为数据
    client.init();

    //阻止a标签冒泡
    $('.leftWrap').on("click", '.content-item a', function(){
        stopEvent('click');
    });
    $(".productInfo a,.articleT a,.related_article a,.productItem a,.goods_info a").on('click', function(event) {
        stopEvent('click');
    });

    switch (window.cateData.pageModule) {
        case 'index':
            pageIndex.init();
            break;
        case 'industry':
            break;
        case 'tank':
            break;
        case 'program':
            break;
    }

    //处理静态数据中的时间
    handleTime();

    //异步处理收藏状态，评论数，收藏数
    gettatInfo();
};
pageInit();

//获取到用户信息后执行
function getUserInfoDone(s) { //获取到用户信息true 未获取到 false
    filelist.init(s);
}

//显示器分辨率
// alert(screen.height);
// alert(screen.width);