var urlRecord = '',
    titleRecord = '',
    needLoginRecord = '',
    articleIdRecord = '',
    fileIdsRecord = '',
    filelist = {
        //预览功能 如果不传articleId 则弹窗的标题不会添加链接
        preview: function (url, title, needLogin, articleId, fileIds, fileName, articleUrl, hiddenDown) {
            //记录参数
            urlRecord = url;
            titleRecord = title;
            needLoginRecord = needLogin;
            articleIdRecord = articleId;
            fileIdsRecord = fileIds;

            //阻止冒泡
            stopEvent('click');

            var isNeedLogin = parseInt(needLogin) || false;

            //判断是否需要登录---预览不需要登录判断
            // if (isNeedLogin) {
            //     if (!isLogin()) {
            //         //跳转登录
            //         window.location.href = window.cateData.url.loginAndBack;
            //         return;
            //     }
            // }

            //用户行为
            userRecode({behaviorType: '预览资料', behaviorResult: fileName,});

            //打开预览窗口
            if(url.indexOf('https') > -1){
                url = url.replace("https","http");
            }
            var previewUrl = '//ow365.cn/?i=16574&furl=' + url;
            msg.filePreviewDialog(previewUrl, title, needLogin, articleId, fileIds, fileName, articleUrl, hiddenDown);
        },

        //预览弹窗下载功能
        // popupDownload: function () {
        //     this.download(urlRecord,titleRecord,needLoginRecord,articleIdRecord,fileIdsRecord);
        // },

        //界面下载功能
        download: function (fileIds, articleId, title, needLogin, fileName) {
            //阻止冒泡
            stopEvent('click');

            var isNeedLogin = parseInt(needLogin) || false;

            var fid = fileIds || '';

            gio('track', 'Filedownload_click', {'consultEntrancePage': document.title , 'consultEntrancePageURL': document.URL, 'FilesTitle_Fifth':title, 'FilesID_Fifth':fid});  //埋点实施方案

            //判断是否需要登录
            if (isNeedLogin) {
                if (!isLogin()) {
                    //用户行为
                    userRecode({behaviorType: '下载资料', behaviorResult: '无',});

                    //将参数存入本地存贮
                    try {
                        localStorage.setItem('dialogType', 'download');
                        localStorage.setItem('fileIds', fileIds);
                        localStorage.setItem('articleId', articleId);
                        localStorage.setItem('title', title);
                        localStorage.setItem('needLogin', needLogin);
                        localStorage.setItem('fileName', fileName);
                    }catch (e) {
                        console.log('localStorage 已满请清理', e);
                    }

                    //跳转登录
                    // window.location.href = window.cateData.url.loginAndBack;
                    window.location.href = "https://www.dellemc-solution.com/personcenter/login.html?backUrl="+ encodeURIComponent(window.location.href);
                    return;
                }
            }

            var email = '';
            if(getCookie('smarketMember_' + window.cateData.schemaId)){
                //登陆后获取注册邮箱
                email = JSON.parse(getCookie('smarketMember_' + window.cateData.schemaId)).email;
            }

            msg.dialogSedfile('', email, sendFile);

            //参数
            var option = {
                articleId: articleId,
                fileList:[fid],
                email:email,
                forEmailTemp: false,
                emailSubject: window.cateData.email.emailSubject,
                senderEmail: window.cateData.email.senderEmail,
                senderName: window.cateData.email.senderName,
                sess: window.cateData.userInfo.sess,
                openId: getUserInfo.options.openId,
                referenceUrl: window.location.href,
                instanceId: 0,
                moduleType: 0,
                extra:{
                    tenantId: window.cateData.tenantId,
                    instanceId: 0,
                    memberId: window.cateData.userInfo.memberId,
                    moduleId: 0,
                    openId: getUserInfo.options.openId,
                    weChatId: window.cateData.weChatId,
                    objInstanceId: 0,
                    source: window.cateData.device,
                },
                globalUserId: getUserInfo.options.globalUserId,
                browseInfo: client.browseInfo,
            };

            function sendFile(sendEmail) {
                option.email = sendEmail;
                postJson(window.cateData.api.downloadWithEmail, option, function (data) {
                    if (data.body.result !== 0) {
                        msg.toast(data.body.desc);
                        //用户行为
                        userRecode({behaviorType: '下载资料', behaviorResult: '下载失败'});
                    } else {
                        //用户行为
                        userRecode({behaviorType: '下载资料', behaviorResult: fileName});
                        msg.toastSedfile();
                    }
                })
            }
        },

        //视频播放
        video: function (url, title, needLogin, articleId, articleUrl,autoplay) {

            //阻止冒泡
            stopEvent('click');

            var isNeedLogin = parseInt(needLogin) || false;

            //判断是否需要登录
            if (isNeedLogin) {
                if (!isLogin()) {
                    //跳转登录
                    //用户行为
                    userRecode({behaviorType: '观看视频', behaviorResult: '无',});

                    //将参数存入本地存贮
                    try {
                        localStorage.setItem('dialogType', 'video');
                        localStorage.setItem('url', url);
                        localStorage.setItem('articleId', articleId);
                        localStorage.setItem('articleUrl', articleUrl);
                        localStorage.setItem('title', title);
                        localStorage.setItem('needLogin', needLogin);
                    }catch (e) {
                        console.log('localStorage 已满请清理', e);
                    }


                    // window.location.href = window.cateData.url.loginAndBack;
                    window.location.href = "https://www.dellemc-solution.com/personcenter/login.html?backUrl="+ encodeURIComponent(window.location.href);
                    return;
                }
            }

            //用户行为
            userRecode({behaviorType: '观看视频', behaviorResult: title,});
            msg.videoDialog(url, title, articleId, articleUrl,autoplay);
        },

        //从参数启动视频弹窗
        initVideo: function () {
            /*var url = getUrlPara('url')
            var title = getUrlPara('title')
            var needLogin = getUrlPara('needLogin')
            var articleId = getUrlPara('articleId')*/

            var url = localStorage.getItem('url');
            var title = localStorage.getItem('title');
            var needLogin = localStorage.getItem('needLogin');
            var articleId = localStorage.getItem('articleId');
            var articleUrl = localStorage.getItem('articleUrl');

            this.video(url, title, needLogin, articleId, articleUrl);
            this.clearStorage('dialogType', 'url', 'articleId', 'title', 'needLogin','articleUrl');
        },

        //从参数启动下载弹窗
        initDown: function () {
            var fileIds = localStorage.getItem('fileIds');
            var title = localStorage.getItem('title');
            var needLogin = localStorage.getItem('needLogin');
            var articleId = localStorage.getItem('articleId');
            var fileName = localStorage.getItem('fileName');
            this.download(fileIds, articleId, title, needLogin, fileName);
            this.clearStorage('dialogType', 'fileIds', 'articleId', 'title', 'needLogin','fileName');
        },

        //本地存储数据
        clearStorage: function () {
            var paArr = arguments;
            for(var i=0; i<paArr.length; i++){
                // delUrlPara(paArr[i]);
                localStorage.removeItem(paArr[i]);
            }
        },

        //初始化
        init: function (s) {
            //阻止冒泡
            $(".filedown-box").on('click', function (event) {
                stopEvent('click');
            });
            if(s){
                //判断是否为登录跳回页面，获取地址栏参数，自动打开下载弹窗
                var dialogType = localStorage.getItem('dialogType');
                if (dialogType) {
                    switch (dialogType) {
                        case 'video':
                            this.initVideo();
                            break;
                        case 'download':
                            this.initDown();
                            break;
                        default:
                            break;
                    }
                }
            }else{
                this.clearStorage('dialogType', 'fileIds', 'articleId', 'title', 'needLogin','fileName', 'url', 'articleUrl');
            }
        }
    };