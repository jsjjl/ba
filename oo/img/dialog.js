//弹出信息
var msg = {

    //弹窗
    dialogBoxEl: document.querySelector('.act-dialog-box'),
    //toast
    toastBoxEl: document.querySelector('.act-toast-box'),

    //发送文件弹窗
    sedfileDialogBoxEl: document.querySelector('.act-sendfile-dialog'),
    //发送文件成功toast
    sedfileToastBoxEl: document.querySelector('.act-sendfile-toast-box'),

    //预览文件弹窗
    filePreviewBoxEl: document.querySelector('.act-file-preview-dialog'),
    videoBoxEl: document.querySelector('.dialog-video-box'),
    bargainEl: document.querySelector('.m-dialog-box'),
    loginEl: document.querySelector('#codeLogin'),

    //toast
    toast: function (text, time) {
        var _this = this;

        var toastBoxEl = _this.toastBoxEl;
        var toastContentEL = toastBoxEl.querySelector('.toast-content');

        var s = time || 1500;

        toastContentEL.innerText = text ? text : '';

        show();

        setTimeout(function () {
            close();
        }, s);

        function show() {
            toastBoxEl.style.display = 'block';
            // toastBoxEl.classList.remove('dialog-mask-ani-fade-out');
            // toastBoxEl.classList.add('dialog-mask-ani-fade-in');
        }

        function close() {
            // toastBoxEl.classList.remove('dialog-mask-ani-fade-in');
            // toastBoxEl.classList.add('dialog-mask-ani-fade-out');
            setTimeout(function () {
                toastBoxEl.style.display = 'none';
            }, 230);
        }

    },

    //提示窗没有取消按钮
    alert: function (text, confirm, cancel, title, isAlert) {
        this.dialog(text, confirm, cancel, title, true);
    },

    //提示窗
    dialog: function (text, confirm, cancel, title, isAlert) {

        var _this = this;
        var dialogBoxEl = _this.dialogBoxEl;

        var dialogEl = dialogBoxEl.querySelector('.dialog');
        var maskEl = dialogBoxEl.querySelector('.dialog-mask');
        var contentEL = dialogBoxEl.querySelector('.dialog-content');
        var titleEL = dialogBoxEl.querySelector('.dialog-title');
        var confirmEl = dialogBoxEl.querySelector('.act-dialog-bt-confirm');
        var cancelEl = dialogBoxEl.querySelector('.act-dialog-bt-cancel');
        var closeEl = dialogBoxEl.querySelector('.act-dialog-close');


        if (typeof confirm !== "function") {
            confirm = function () {
            };
        }
        if (typeof cancel !== "function") {
            cancel = function () {
            };
        }

        //设置信息
        titleEL.innerText = title ? title : '';
        contentEL.innerText = text ? text : '';
        if(isAlert){
            dialogBoxEl.querySelector('.act-dialog-bt-cancel').classList.add('hide');
        }

        //显示
        show();

        confirmEl.addEventListener('click', funcConfirm);

        cancelEl.addEventListener('click', funcCancel);
        closeEl.addEventListener('click', close);
        maskEl.addEventListener('click', close);

        // 确认
        function funcConfirm() {
            confirm();
            close();
        }


        // 取消
        function funcCancel() {
            cancel();
            close();
        }

        function show() {

            dialogBoxEl.style.display = 'flex';
            maskEl.classList.remove('dialog-mask-ani-fade-out');
            dialogEl.classList.remove('dialog-ani-to-top');
            maskEl.classList.add('dialog-mask-ani-fade-in');
            dialogEl.classList.add('dialog-ani-from-top');

            //禁止后方滚动
            setScroll.unScroll();
        }

        function close() {
            maskEl.classList.remove('dialog-mask-ani-fade-in');
            maskEl.classList.add('dialog-mask-ani-fade-out');

            dialogEl.classList.remove('dialog-ani-from-top');
            dialogEl.classList.add('dialog-ani-to-top');
            setTimeout(function () {
                dialogBoxEl.style.display = 'none';
            }, 230);

            //移除事件
            confirmEl.removeEventListener('click', funcConfirm);

            cancelEl.removeEventListener('click', funcCancel);
            closeEl.removeEventListener('click', close);
            maskEl.removeEventListener('click', close);

            //允许后方滚动
            setScroll.removeUnScroll();
        }
    },

    //发送文件弹窗
    dialogSedfile: function (text, email, confirm, cancel, title) {

        var _this = this;
        var dialogBoxEl = _this.sedfileDialogBoxEl;

        var dialogEl = dialogBoxEl.querySelector('.dialog');
        var maskEl = dialogBoxEl.querySelector('.dialog-mask');
        var contentEL = dialogBoxEl.querySelector('.dialog-content');
        var titleEL = dialogBoxEl.querySelector('.dialog-title');
        var confirmEl = dialogBoxEl.querySelector('.act-dialog-bt-confirm');
        var cancelEl = dialogBoxEl.querySelector('.act-dialog-bt-cancel');
        var closeEl = dialogBoxEl.querySelector('.act-dialog-close');
        var inputEl = dialogBoxEl.querySelector('.dialog-input>input');
        var errorEl = dialogBoxEl.querySelector('.dialog-input-error');


        if (typeof confirm !== "function") {
            confirm = function () {
            };
        }
        if (typeof cancel !== "function") {
            cancel = function () {
            };
        }

        //设置信息
        titleEL.innerText = title ? title : '';
        if(text){
            contentEL.innerText = text;
        }
        inputEl.value = email;

        //显示
        show();

        confirmEl.addEventListener('click', send);

        closeEl.addEventListener('click', close);
        maskEl.addEventListener('click', close);

        //监听邮箱输入框
        inputEl.addEventListener('keyup', verificationEmail);

        //确认发送
        function send() {
            if(!verificationEmail(inputEl.value)){
                return;
            }
            confirm(inputEl.value);
            close();
        }

        //验证邮箱
        function verificationEmail() {
            var email = inputEl.value;
            if(!email.match(/^\w+[\w\-\.]*@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{2,})+$/)){
                inputEl.style.border = '1px solid #ff5959';
                errorEl.style.display = 'block';
                return false;
            }else{
                inputEl.style.border = '1px solid #e9eef4';
                errorEl.style.display = 'none';
                return true;
            }
        }

        function show() {

            dialogBoxEl.style.display = 'flex';
            maskEl.classList.remove('dialog-mask-ani-fade-out');
            dialogEl.classList.remove('dialog-ani-to-top');
            maskEl.classList.add('dialog-mask-ani-fade-in');
            dialogEl.classList.add('dialog-ani-from-top');

            //禁止后方滚动
            setScroll.unScroll();
        }

        function close() {
            maskEl.classList.remove('dialog-mask-ani-fade-in');
            maskEl.classList.add('dialog-mask-ani-fade-out');

            dialogEl.classList.remove('dialog-ani-from-top');
            dialogEl.classList.add('dialog-ani-to-top');
            setTimeout(function () {
                dialogBoxEl.style.display = 'none';
                errorEl.style.display = 'none';
                inputEl.style.border = '1px solid #e9eef4';
            }, 230);

            //移除事件
            confirmEl.removeEventListener('click', send);
            closeEl.removeEventListener('click', close);
            maskEl.removeEventListener('click', close);

            //允许后方滚动
            setScroll.removeUnScroll();
        }
    },


    //发送文件成功toast
    toastSedfile: function (text, time) {
        var _this = this;

        var toastBoxEl = _this.sedfileToastBoxEl;
        var toastContentEL = toastBoxEl.querySelector('.toast-text');

        var s = time || 1500;

        if(text){
            toastContentEL.innerText = text;
        }

        show();

        setTimeout(function () {
            close();
        }, s);

        function show() {
            toastBoxEl.style.display = 'block';
            // toastBoxEl.classList.remove('dialog-mask-ani-fade-out');
            // toastBoxEl.classList.add('dialog-mask-ani-fade-in');
        }

        function close() {
            // toastBoxEl.classList.remove('dialog-mask-ani-fade-in');
            // toastBoxEl.classList.add('dialog-mask-ani-fade-out');
            setTimeout(function () {
                toastBoxEl.style.display = 'none';
            }, 230);
        }

    },

    //预览文件弹窗
    filePreviewDialog: function (url, title, needLogin, articleId, fileIds, fileName, articleUrl, hiddenDown) {
        var _this = this;
        var dialogBoxEl = _this.filePreviewBoxEl;

        var dialogEl = dialogBoxEl.querySelector('.dialog-filepre-content');
        var maskEl = dialogBoxEl.querySelector('.dialog-mask');
        var titleEL = dialogBoxEl.querySelector('.file-action-bar>.title>a>h2');
        var titleLinkEL = dialogBoxEl.querySelector('.file-action-bar>.title>a');
        var closeEl = dialogBoxEl.querySelector('.closeicon');
        var iframeEl = dialogBoxEl.querySelector('.file-preview-box>iframe');
        var likeEl = dialogBoxEl.querySelector('.like');
        var likeIconEl = dialogBoxEl.querySelector('.like-icon');
        var downEl = dialogBoxEl.querySelector('.downicon');
        var collectType = $('.act-like-'+ articleId).attr('data-status');

        //动态设置iframe内容高度
        var iframeBoxEl = dialogBoxEl.querySelector('.file-preview-box');
        // var h = window.innerHeight * 0.85;
        // iframeBoxEl.style.height = h + 'px';

        //设置点赞状态
        getCollectionStatus(articleId, likeEl);
        //如果articleId 不存在则隐藏点赞按钮
        if(!articleId){
            likeEl.classList.add('hide');
        }

        //文件点赞功能
        likeEl.classList.add('act-like-'+articleId);
        likeEl.setAttribute('data-id', articleId);
        likeEl.setAttribute('data-title', title);
        likeEl.setAttribute('data-status', collectType);
        likeIconEl.classList.add('status-like-'+collectType);

        //设置信息
        if(title === '0'){
            title = '';
        }

        if(hiddenDown === '1'){
            downEl.classList.add('hide');
        }else{
            downEl.classList.remove('hide');
        }

        titleEL.innerText = title ? title : '';
        iframeEl.setAttribute('src', url);

        //设置标题跳转链接
        if(articleUrl){
            titleLinkEL.setAttribute('target', '_blank');
            titleLinkEL.setAttribute('href', articleUrl);
        }else if(articleId && articleId !== '0' && window.cateData.pageModule !== 'detail'){
            titleLinkEL.setAttribute('target', '_blank');
            titleLinkEL.setAttribute('href', window.urlConfig.type + window.urlConfig.baseUrl + '/view-' + articleId + '.html');
        }else{
            titleLinkEL.setAttribute('href', 'javascript:void(0)');
        }



        //显示
        show();

        closeEl.addEventListener('click', close);
        maskEl.addEventListener('click', close);

        downEl.addEventListener('click' , down);

        // 下载
        function down() {
            filelist.download(fileIds, articleId,title, needLogin, fileName);
        }

        function show() {

            dialogBoxEl.style.display = 'flex';
            maskEl.classList.remove('dialog-mask-ani-fade-out');
            dialogEl.classList.remove('dialog-ani-to-top');
            maskEl.classList.add('dialog-mask-ani-fade-in');
            dialogEl.classList.add('dialog-ani-from-top');

            //禁止后方滚动
            setScroll.unScroll();
        }

        function close() {
            maskEl.classList.remove('dialog-mask-ani-fade-in');
            maskEl.classList.add('dialog-mask-ani-fade-out');

            dialogEl.classList.remove('dialog-ani-from-top');
            dialogEl.classList.add('dialog-ani-to-top');
            setTimeout(function () {
                dialogBoxEl.style.display = 'none';
                //关闭后将url置空
                iframeEl.setAttribute('src', '');
                titleLinkEL.setAttribute('href', 'javascript:void(0)');
                //判断是否是我喜爱的内容弹窗
                if(window.cateData.pageSubModule == 'favorite' && $.cookie('cancelCollect')){
                    $.cookie('cancelCollect', '', { expires: -1 }); // 删除 cookie
                    window.location.reload();
                }
            }, 230);

            //移除事件
            closeEl.removeEventListener('click', close);
            maskEl.removeEventListener('click', close);
            downEl.removeEventListener('click' , down);

            //允许后方滚动
            setScroll.removeUnScroll();
        }
    },

    //预览视频弹窗
    videoDialog: function (url, title, articleId, articleUrl,autoPlay) {
        var _this = this;
        var dialogBoxEl = _this.videoBoxEl;

        var dialogEl = dialogBoxEl.querySelector('.dialog-video-content');
        var maskEl = dialogBoxEl.querySelector('.dialog-mask');
        var titleEL = dialogBoxEl.querySelector('.video-action-bar>.title>a>h2');
        var titleLinkEL = dialogBoxEl.querySelector('.video-action-bar>.title>a');
        var closeEl = dialogBoxEl.querySelector('.closeicon');
        var videoEl = dialogBoxEl.querySelector('.video-box>video');
        var likeEl = dialogBoxEl.querySelector('.like');
        var likeIconEl = dialogBoxEl.querySelector('.like>.like-icon');
        var collectType = $('.act-like-'+ articleId).attr('data-status');

        //设置点赞状态
        getCollectionStatus(articleId, likeEl);

        //如果articleId 不存在则隐藏点赞按钮
        if(!articleId){
            likeEl.classList.add('hide');
        }

        //视频点赞功能
        likeEl.classList.add('act-like-'+articleId);
        likeEl.setAttribute('data-id', articleId);
        likeEl.setAttribute('data-title', title);
        likeEl.setAttribute('data-status', collectType);
        likeIconEl.classList.add('status-like-'+collectType);

        //设置信息
        titleEL.innerText = title ? title : '';
        videoEl.setAttribute('src', url);

        //设置标题跳转链接
        if(articleUrl){
            titleLinkEL.setAttribute('target', '_blank');
            titleLinkEL.setAttribute('href', articleUrl);
        }else if(articleId){
            titleLinkEL.setAttribute('target', '_blank');
            titleLinkEL.setAttribute('href', window.urlConfig.type + window.urlConfig.baseUrl + '/view-' + articleId + '.html');
        }else{
            titleLinkEL.setAttribute('href', 'javascript:void(0)');
        }


        //显示
        show();

        closeEl.addEventListener('click', close);
        maskEl.addEventListener('click', close);

        function show() {

            dialogBoxEl.style.display = 'flex';
            maskEl.classList.remove('dialog-mask-ani-fade-out');
            dialogEl.classList.remove('dialog-ani-to-top');
            maskEl.classList.add('dialog-mask-ani-fade-in');
            dialogEl.classList.add('dialog-ani-from-top');

            //Google、Safari 兼容自动播放
            if (autoPlay && (navigator.userAgent.indexOf('Chrome') > -1 || navigator.userAgent.indexOf('Safari') > -1)){
                //如果是谷歌浏览器
                setTimeout(function () {
                    if (!videoEl.paused){
                        return;
                    }
                    console.log("fhsodfhs",videoEl.paused);
                    msg.dialog('是否允许本页中的音/视频自动播放？',function () {
                        let timer=null;
                        function autoPlay() {
                            console.log(videoEl.paused);
                            if (videoEl.paused) { //如果暂停
                                videoEl.paused=false;//设为不暂停
                                videoEl.play();//调用play（）方法播放
                            }else {//如果未暂停
                                clearInterval(timer);//清掉定时器
                            }
                        }
                        timer=setInterval(autoPlay,1000);//用定时器触发autoplay自动播放
                    },function () {
                        close();
                    },'提示信息：')
                },1000)


            }else{
                videoEl.play();
            }


            //禁止后方滚动
            setScroll.unScroll();
        }

        function close() {
            maskEl.classList.remove('dialog-mask-ani-fade-in');
            maskEl.classList.add('dialog-mask-ani-fade-out');

            dialogEl.classList.remove('dialog-ani-from-top');
            dialogEl.classList.add('dialog-ani-to-top');
            setTimeout(function () {
                dialogBoxEl.style.display = 'none';
                //判断是否是我喜爱的内容弹窗
                if(window.cateData.pageSubModule == 'favorite' && $.cookie('cancelCollect')){
                    $.cookie('cancelCollect', '', { expires: -1 }); // 删除 cookie
                    window.location.reload();
                }
            }, 230);

            videoEl.pause();
            videoEl.setAttribute('src', '');
            titleLinkEL.setAttribute('href', 'javascript:void(0)');

            closeEl.removeEventListener('click', close);
            maskEl.removeEventListener('click', close);

            //允许后方滚动
            setScroll.removeUnScroll();
        }
    },

    
    // 议价弹窗
    bargainDialog: function (urlData, formConfig) {
        var _this = this;

        //如果没有传过来配置，则用批量议价单的配置
        var formConfig = formConfig ||  window.cateData.batchFrome;

        //设置产品信息
        var productInfoDef = {
            productname: '产品名称',
            productImg:'/static/mobile/images/productimgdef.png',
            productDesc:'产品介绍',
            productnum: '1',
            productprice: '6499.00',
            productid: '123',
        };
        var info = $.extend(productInfoDef, urlData);

        var dialogBoxEl = _this.bargainEl;

        var dialogEl = dialogBoxEl.querySelector('.m-bargain-content');
        var maskEl = dialogBoxEl.querySelector('.dialog-mask');
        var closeEl = dialogBoxEl.querySelector('.close-modal');
        var confirmEl = dialogBoxEl.querySelector('.submit-bargain-btn');

        //显示
        show();

        closeEl.addEventListener('click', close);
        maskEl.addEventListener('click', close);
        confirmEl.addEventListener('click', send);

        var bargainProName = document.querySelector('.bargain-pro-name');
        var bargainProDesc = document.querySelector('.bargain-pro-desc');
        var bargainProImg = document.querySelector('.bargain-pro-picture img');
        var bargainProPrice = document.querySelector('.bargain-pro-buy .cur-price');
        var bargainProNum = document.querySelector('.j-num-input2');
        var bargainPriceLabel = document.querySelector('.j-price-label');
        var bargainDelPrice = document.querySelector('.del-price');
        var bargainFormTitle = document.querySelector('.dialog-bargain-title');
        bargainProImg.src = urlData.productImg || '';
        bargainProNum.value = urlData.productnum || '';
        bargainProName.innerText = urlData.productname || '';
        bargainProDesc.innerText = urlData.productDesc || '';
        bargainProPrice.innerText = urlData.productprice || '';
        bargainPriceLabel.innerText = urlData.salePrice ? '促销价格' : '参考价格';
        bargainFormTitle.innerText = formConfig.formName || '';
        if (urlData.salePrice && urlData.unSalePrice) {
            bargainDelPrice.innerText = urlData.unSalePrice;
            bargainDelPrice.classList.remove('hide');
        }
        bargainProImg.setAttribute('alt', urlData.productname);
        var numIn = numInput('.j-num-input2', '.j-plus02', '.j-minus02');
        numIn.setInputVal(urlData.productnum);

        // 初始化表单
        var formName = new HandelValue('dialogName', 'dialogNameV','dialogNameVE', 'form-error', '请输入姓名');
        var formMobile = new HandelValue('dialogMobile', 'dialogMobileV','dialogMobileVE','form-error', '请输入手机', '输入正确的手机号', baseVerif.isMobile);
        var formEmail = new HandelValue('dialogEmail', 'dialogEmailV','dialogEmailVE','form-error', '请输入邮箱', '输入正确的邮箱', baseVerif.isEmail);
        var formCompName = new HandelValue('dialogCompName','dialogCompV','dialogCompVE','form-error', '请输入公司名称');

        //设置表单默认值
        formName.set(getUserInfo.options.name || '');
        formMobile.set(getUserInfo.options.mobile || '');
        formEmail.set(getUserInfo.options.email || '');
        formCompName.set(getUserInfo.options.enterpriseName || '');

        function show() {
            dialogBoxEl.style.display = 'flex';
            maskEl.classList.remove('dialog-mask-ani-fade-out');
            dialogEl.classList.remove('dialog-ani-to-top'
            );
            maskEl.classList.add('dialog-mask-ani-fade-in');
            dialogEl.classList.add('dialog-ani-from-top');

            //禁止后方滚动
            setScroll.unScroll();
        }

        function close() {
            maskEl.classList.remove('dialog-mask-ani-fade-in');
            maskEl.classList.add('dialog-mask-ani-fade-out');

            dialogEl.classList.remove('dialog-ani-from-top');
            dialogEl.classList.add('dialog-ani-to-top');

            bargainDelPrice.classList.add('hide');

            setTimeout(function () {
                dialogBoxEl.style.display = 'none';
            }, 230);
            closeEl.removeEventListener('click', close);
            maskEl.removeEventListener('click', close);
            confirmEl.removeEventListener('click', send);

            //允许后方滚动
            setScroll.removeUnScroll();
            if (numIn) {
                numIn.destroy();
            }
        }

        //确认发送
        function send() {
            //进行验证
            formName.change();
            formMobile.change();
            formEmail.change();
            formCompName.change();
            if(formName.isVerif && formMobile.isVerif && formEmail.isVerif && formCompName.isVerif){
                //进行验证
                var url = window.cateData.api.actionForm;
                var parainfo = {
                    customFormId:formConfig.formId,
                    globalUserId:getUserInfo.options.globalUserId,
                    instanceId:'',
                    items:[
                        {fieldName: "enterprise_name", value: formCompName.get()},
                        {fieldName: "name", value: formName.get()},
                        {fieldName: "mobile", value: formMobile.get()},
                        {fieldName: "email", value: formEmail.get()},
                        {fieldName: formConfig.customFilds.productName, value: info.productname},
                        {fieldName: formConfig.customFilds.productId, value: info.productid},
                        {fieldName: formConfig.customFilds.productPrice, value: info.productprice},
                        {fieldName: formConfig.customFilds.productNum, value: numIn.getInputVal()},
                        {fieldName: "linksource", value: window.location.href},
                    ],
                    linkId:'',
                    sess: window.cateData.userInfo.sess,
                    referenceUrl:'',
                    url:window.location.href,
                    ver: "v2.0.1",
                };
                postJson(url, parainfo, function (data) {
                    if(data.body.result === 0){
                        userRecode({behaviorType: formConfig.behaviorType, behaviorResult: info.productName});
                        msg.toastSedfile('提交成功！');
                    }else{
                        userRecode({behaviorType: formConfig.behaviorType, behaviorResult: '提交失败'});
                        msg.toast('提交失败,请稍后再试');
                    }
                });
                gio('track', formConfig.gioType, {'ibuyEntrancePageTitle': document.title, 'ibuyEntrancePageURL': document.URL, 'productID':info.productId, 'productname':info.productName, 'linkSourceurl': window.location.href, 'company':formCompName.get(), 'email': formEmail.get(), 'userName': formName.get(), 'phoneNumber':formMobile.get()});  //埋点实施方案
                close();
            }
        }
    },
    
    // 验证码登录弹窗
    codeLoginDialog: function (code) {
        var _this = this;
        var dialogBoxEl = _this.loginEl;
        var dialogEl = dialogBoxEl.querySelector('.m-login-content');
        var maskEl = dialogBoxEl.querySelector('.dialog-mask');
        var inputEl = dialogBoxEl.querySelector('.form-control-input');
        var confirmEl = dialogBoxEl.querySelector('.dialog-confirm-btn');
        var errorTip = dialogBoxEl.querySelector('.error-tip');

        confirmEl.addEventListener('click', login);

        show();
        function show() {
            dialogBoxEl.style.display = 'flex';
            maskEl.classList.remove('dialog-mask-ani-fade-out');
            dialogEl.classList.remove('dialog-ani-to-top');
            maskEl.classList.add('dialog-mask-ani-fade-in');
            dialogEl.classList.add('dialog-ani-from-top');

            //禁止后方滚动
            setScroll.unScroll();
        }

        function close() {
            maskEl.classList.remove('dialog-mask-ani-fade-in');
            maskEl.classList.add('dialog-mask-ani-fade-out');

            dialogEl.classList.remove('dialog-ani-from-top');
            dialogEl.classList.add('dialog-ani-to-top');

            setTimeout(function () {
                dialogBoxEl.style.display = 'none';
                window.location.reload();
            }, 230);
            confirmEl.removeEventListener('click', login);

            //允许后方滚动
            setScroll.removeUnScroll();
        }

        function login() {
            if (!inputEl.value || $.trim(inputEl.value).toUpperCase() != code.toUpperCase()) {
                errorTip.innerText = inputEl.value ? '*验证码无效' : '*请输入验证码';
                errorTip.style.display = 'block';
                return;
            }
            $.cookie('verify_code', inputEl.value, { expires: 7, domain: '.dellemc-solution.com'});
            close();
        }
    }
};