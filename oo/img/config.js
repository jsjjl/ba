window.config = window['localhost'] = {
    defaultMeetingBg: "../public/img/defaultbanner/listbanner.jpg",  //默认会议背景图片
    //默认会议详情banner图5套
    defaultMeetingInfoBanner: [
        {pc: "../public/img/defaultbanner/pcbanner1.jpeg", wap: "../public/img/defaultbanner/wapbanner1.jpeg"},
        {pc: "../public/img/defaultbanner/pcbanner2.jpeg", wap: "../public/img/defaultbanner/wapbanner2.jpeg"},
        {pc: "../public/img/defaultbanner/pcbanner3.jpeg", wap: "../public/img/defaultbanner/wapbanner3.jpeg"},
        {pc: "../public/img/defaultbanner/pcbanner4.jpeg", wap: "../public/img/defaultbanner/wapbanner4.jpeg"},
        {pc: "../public/img/defaultbanner/pcbanner5.jpeg", wap: "../public/img/defaultbanner/wapbanner5.jpeg"},
    ],
    //跳转目录（seo）
    jumpUrl: [
        {fileUrl: 'notebook.html', id: '150042', className: "Latitude商用笔记本"},
        {fileUrl: 'notebook-latitude.html', id: '150042', className: "Latitude商用笔记本"},

        {fileUrl: 'computer.html', id: '150041', className: "OptiPlex"},
        {fileUrl: 'computer-OptiPlex.html', id: '150041', className: "OptiPlex"},
        {fileUrl: 'computer-chengming.html', id: '150041', className: "成铭"},

        {fileUrl: 'workstation.html', id: '150040', className: "Precision移动工作站"},
        {fileUrl: 'workstation-precision-laptops.html', id: '150040', className: "Precision移动工作站"},
        {fileUrl: 'workstation-precision-desktops.html', id: '150040', className: "Precision台式工作站"},

        {fileUrl: 'server.html', id: '150039', className: "塔式服务器"},
        {fileUrl: 'server-tower-servers.html', id: '150039', className: "塔式服务器"},
        {fileUrl: 'server-rack-servers.html', id: '150039', className: "机架式服务器"},
        {fileUrl: 'server-modular-infrastructure.html', id: '150039', className: "模块化基础架构"},

        {fileUrl: 'storage.html', id: '150038', className: "数据存储",},
        {fileUrl: 'storage-data-storage.html', id: '150038', className: "数据存储"},
        {fileUrl: 'storage-data-protection.html', id: '150038', className: "数据保护"},
        {fileUrl: 'storage-converged-infrastructure.html', id: '150038', className: "融合架构"},

        {fileUrl: 'networking.html', id: '150037', className: "交换机"},
        {fileUrl: 'networking-switches.html', id: '150037', className: "交换机"},
        {fileUrl: 'networking-Ethernet-switch.html', id: '150037', className: "可管理快速以太网交换机"},
        {fileUrl: 'networking-management-and-software.html', id: '150037', className: "网络管理和软件"}
    ],
    //自定义职务列表(表单职务字段字典表)
    positionList: {
        gc: ["请选择", "CEO", "CFO", "CIO", "CISO", "CMO", "COO", "CTO"],
        hq: ["请选择", "后勤/设备/公共设施"],
        cw: ["请选择", "财务总监", "财务经理", "财务分析", "财务总管"],
        rl: ["请选择", "人力资源总监", "人力资源经理", "人力资源专员"],
        it: ["请选择", "IT总监", "IT经理", "IT顾问", "工程部经理", "项目经理", "LAN/WAN经理", "解决方案架构师", "系统工程师", "服务器经理", "数据库经理", "服务交付", "系统经理", "技术经理", "系统管理员"],
        sc: ["请选择", "市场总监", "市场经理", "市场分析师", "程序协调员"],
        yy: ["请选择", "合同主管", "物料支持", "运营经理", "程序/项目经理", "质量控制经理", "生产总监"],
        cg: ["请选择", "合规经理", "采购部总监", "采购部经理", "采购部代表", "公开招标经理"],
        xs: ["请选择", "客户代表", "销售总监", "销售经理", "销售代表", "区域经理"],
        px: ["请选择", "培训总监", "培训经理", "培训协调员"],
        qt: ["请选择", "其它"]
    },
    "cookie": {
        "domain": ".dellemc-solution.com",
        "json": true,
        "flag": {
            "member": "smarketMember",
            "openId": "openId",
            "customForm": "smarketCustomForm",
            "email": "email",
            "increase": "increase" //补全信息
        }
    },
    "componentAppId": 'wxc73c91befc451582',
    weChatId: 34443,//正式服务号戴尔易安信市场活动
    tenantId: 444,
    schemaId: 498,
    rigisterFormId: 1386,
    rigisterVisitorFormId: 1387,
    rigisterFields: {
        areacode: "areacode",//区号
        compphone: "compphone",//座机
        ext: "ext",//分机
        contactAllow: "ifWish",//联系字段fieldname
        privacynotice: "privacynotice",//隐私政策fieldname
        equipment: "equipment",//设备类型
        platform: "platform",//设备系统
        browser: "browser",
        IP: "dellRegisterIp",
        link_source: "linksource",//页面来源
        link_sourcemodule: "linksourcemodule",//页面来源模块
        mediasources: "mediaSources",//推广来源
        channelsources: "channelsources",//注册来源
        createTime: "dellRegisterTime",
        title: "title",
        url: "url"
    },
    cacheSettings: {    //缓存接口列表
        interfaces: [
            '/dic/params/getTree',
            '/api/article/get',
            '/api/article/query',
            '/articleCategory/getList',
            '/file/folder/getReleaseFile',
            '/api/webinar/query',
            '/api/seminar/query',

        ],
        cacheSwitch: true, //是否全局启用本地缓存
        localCacheSwitch: true, //是否全局启用本地缓存
        cacheExp: 180, //过期时间(秒)
        refresh: false //是否强制刷新
    },
    //customFormId: 846,//销售意向单表单
    customFormId: 1160,//销售意向单表单
    /*customFormHideField: { //销售意向单表单隐藏字段
        equipment: "custom_form_field_1543328975536",//设备类型
        platform: "custom_form_field_1543328976377",//设备系统
        browser: "custom_form_field_1543328978913",//设备浏览器
        link_source: "custom_form_field_1543328984071",//页面来源
        link_sourcemodule: "custom_form_field_1543328984368",//页面来源模块
        mediasources: "custom_form_field_1543328983590",//推广来源
        createTime: "custom_form_field_1539338581942",//报名时间
        title: "custom_form_field_1543328984729",//页面title
        url: "custom_form_field_1543328985319" //页面url
    }*/
    customFormHideField: { //销售意向单表单隐藏字段
        equipment: "custom_form_field_1545042040534",//设备类型
        platform: "custom_form_field_1545040569502",//设备系统
        browser: "custom_form_field_1545040583925",//设备浏览器
        link_source: "custom_form_field_1545040592989",//页面来源
        link_sourcemodule: "custom_form_field_1545033265212",//页面来源模块
        mediasources: "custom_form_field_1545042033164",//推广来源
        createTime: "custom_form_field_1539338581942",//报名时间
        title: "custom_form_field_1545033336762",//页面title
        url: "custom_form_field_1545033342863" //页面url
    },

    vistorSeminarInfoUrl: "//www.dellemc-solution.com/seminar/seminarinfo.html",//线下会访员详情页面
    vistorWebinarInfoUrl: "//www.dellemc-solution.com/webinar-live-template/activityinfo.html",//线上会访员直播详情页面
    vistorDemandInfoUrl: "//www.dellemc-solution.com/webinar-demand-template/onDemandInfo.html",//线上会访员点播页面
    vistorwebinarsignup: "//p2.smarket.net.cn/dellfy/visitor/webinar/webinarSignup.html",
    vistorseminarsignup: "//p2.smarket.net.cn/dellfy/visitor/seminar/seminarsignup.html",
    proxyInterfaces: [
        //需要签名
        '/edm/sendEmail'
    ],
    convertSignUrl: function (url) {
        return '//projectsapi.smarket.net.cn/beisensignsmarketapi?smkproxyurl=' + encodeURIComponent(url);
    },

    "api": {
        "proxy": "//projectsapi.smarket.net.cn/beisensignsmarketapi?smkproxyurl=",
        "weChatAuthProxy": "//p1.smarket.net.cn/index.html",
        "cdn": "//cdn.smarket.net.cn",
        'gateway': {
            'wechat': '//hwapi-wechat.smarket.net.cn',
            'webinar': '//hwapi-webinarb.smarket.net.cn',
            'template': '//hwtemplateapi-wechat.smarket.net.cn',
            'dict': '//hwapi-dictb.smarket.net.cn',
            'product': '//hwapi-product.smarket.net.cn',
            'article': '//hwapi-article.smarket.net.cn',
            'seminar': '//hwapi-sdeb.smarket.net.cn',
            'admintool': '//s2-oldapi.smarket.net.cn',
            'account': '//hwapi-account.smarket.net.cn',
            'tools': '//hwapi-tools.smarket.net.cn',
            'core': '//hw-saasapi.smarket.net.cn',
            'file': '//hwapi-file.smarket.net.cn',
            'topic': '//hwapi-topic.smarket.net.cn',
            'pdebUrl': '//hwapi-pdeb.smarket.net.cn',
            'general': '//api.dellemc-solution.com',
            // 'general': '//api-dellnew.smarket.net.cn',
            //'general': '//test-api.smarket.net.cn'
        },
        "ungateway": {
            'sdebUrl': '//hwapi-sdeb.smarket.net.cn/',
            'contentUrl': '//dell-saas.smarket.net.cn/content/index.php'
        }
    },
    coverImageUrl: '//smarket.dellemc-solution.com/content/index.php?mappingId=',
    previewUrl: "//ow365.cn/?i=16574&furl=http://smarket.dellemc-solution.com/content/index.php?isPreview=1&type=download&mappingId=",
    productTag: 'producttest',//产品标签（英文）
    index: {
        articleCategoryId: 150022, //首页
        chirldren: {
            footerLogo: 150036, //底部logo
            exampleCenter: 150035, //案例智库
            meetCenter: 150034, //会议讲座
            productionCenter: 150033, //生产力解决方案
            ITCenter: 150032, //IT 转型解决方案
            specialCenter: 150031, //专题页轮播
            fileCenter: 150029, //资料中心
            newsCenter: 150030, //咨询中心
            video: 150028,
            banner: 150026,
            product: 150027
        }
    },
    solution: {
        articleCategoryId: 150009, //解决方案
    },
    productionsolution: {
        articleCategoryId: 150011, //生产力解决方案
        chirldren: {
            headBanner: 150014, //头部banner
            list: 150013, //产品列表
            footerLogo: 150012 //底部logo
        }
    },
    itsolution: {
        articleCategoryId: 150010, //IT 转型解决方案
        chirldren: {
            headBanner: 150015, //头部banner
            list: 150017, //产品列表
            footerLogo: 150016 //底部logo
        }
    },
    activity: {
        articleCategoryId: 150086, //会议中心
        chirldren: {
            headBanner: 150087, //头部banner
            footerLogo: 150089 //底部logo
        }
    },
    demand: {
        articleCategoryId: 103278, //点播会场
        chirldren: {
            footerLogo: 103279 //底部logo
        }
    },
    demandInfo: {
        articleCategoryId: 150103, //点播详情
        chirldren: {
            headBanner: 150104, //头部banner
            footerLogo: 150105 //底部logo
        }
    },
    datum: {
        articleCategoryId: 150019, //资料中心
        chirldren: {
            datum: 150076, //资料下载
            video: 150077, //视频中心
            banner: 150078, //banner设置
            footerLogo: 150075 //底部logo
        }
    },
    casesolution: {
        articleCategoryId: 150018, //案例智库
        chirldren: {
            headBanner: 150079, //头部banner
            list: 150080, //产品列表
            footerLogo: 150081 //底部logo
        }
    },
    product: {
        articleCategoryId: 150021, //产品中心
        notebook: {
            articleCategoryId: 150042, //笔记本
            headBanner: 150073,
            list: 150072, //产品列表
            footerLogo: 150071 //底部logo
        },
        PC: {
            articleCategoryId: 150041, //一体机
            headBanner: 150068,
            list: 150067, //产品列表
            footerLogo: 150066 //底部logo
        },
        workSpace: {
            articleCategoryId: 150040, //工作站
            headBanner: 150063,
            list: 150062, //产品列表
            footerLogo: 150061 //底部logo
        },
        server: {
            articleCategoryId: 150039, //服务器
            articleCategoryName: '服务器',
            headBanner: 150057,
            list: 150056, //产品列表
            footerLogo: 150055 //底部logo
        },
        layIn: {
            articleCategoryId: 150038, //储存
            headBanner: 150051,
            list: 150050, //产品列表
            footerLogo: 150049 //底部logo
        },
        switchboard: {
            articleCategoryId: 150037, //网络交换机
            headBanner: 150045,
            list: 150044, //产品列表
            footerLogo: 150043 //底部logo
        },
        channel: {
            articleCategoryId: 150020, //渠道资讯中心
            children: {
                headBanner: 150023, //头部banner
                list: 150024, //列表
                chengMing: 150090,//成铭
                workStation: 150092,//工作站
                optiPlex: 150091,//optiPlex
                footerLogo: 150025 //底部logo
            }
        }
    },
    lightReadingWXShareImgId: '205004791',
    lightReading: { //轻阅读
        headBanner: 150162, //头部banner
        list: [
            {
                name: '现代化数据中心',
                list: 150165,
                cover: 150164
            },
            {
                name: '多云',
                list: 150169,
                cover: 150170
            },
            {
                name: '关键业务应用',
                list: 150174,
                cover: 150173
            },
            {
                name: '物联网 IOT',
                list: 150179,
                cover: 150178
            },
            {
                name: '人工智能 AI',
                list: 150191,
                cover: 150190
            },
            {
                name: '产品中心',
                list: 150189,
                cover: 150188
            },
            {
                name: '工程师笔记',
                list: 150187,
                cover: 150186
            },
            {
                name: '黑科技',
                list: 150185,
                cover: 150184
            }

        ], //列表
        footerLogo: 150160 //底部logo
    },
    IndustryReading: { //行业内容阅读
        list: [
            {
                name: '产品中心',
                list:150411
            },
            {
                name: '媒体文章',
                list: 150412
            },
            {
                name: '市场活动',
                list: 150413
            },
            {
                name: '案例智库',
                list: 150414
            },
            {
                name: '解决方案',
                list: 150415
            }
        ], //列表
    },

    tags: {//通用列表分类页每个tab对应标签类别
        video: ['producttest', 'industrytest', 'applicationaArea', 'scale'],
        download: ['producttest', 'industrytest', 'applicationaArea', 'scale'],
        case: ['producttest', 'industrytest', 'applicationaArea', 'scale'],
        online: ['producttest', 'industrytest', 'applicationaArea', 'scale'],//线上会
        unline: ['producttest', 'industrytest', 'applicationaArea', 'scale'],//线下会
        solution: ['producttest', 'industrytest', 'applicationaArea', 'scale']
    },

    email: {
        emailSubject: '戴尔易安信解决方案中心-相关资料下载',
        senderEmail: 'dellEmc@dell.com',
        senderName: '戴尔易安信解决方案中心'
    },
    brodemliveUrl: '//www.dellemc-solution.com/webinar-live-template/brodemlive.html',//直播会场地址必须是全链接
    activityinfoUrl: '../webinar-live-template/activityinfo.html',  //直播详情
    onDemandInfoUrl: '../webinar-demand-template/onDemandInfo.html',  //直播详情
    dianbo: '../webinar-demand-template/onDemandInfo.html',
    loginUrl: '../personcenter/login.html',  //跳转登录地址
    signupUrl: '../webinar/applyonline.html',//跳转报名地址
    personUrl: '../personcenter/personinfo.html?',//跳转个人中心地址
    //yihuiHref:"//tv.ehub.net/dell/index.html",
    yihuiHref: "//www.dellemc-solution.com/MeetYH/",  //重定向地址
    sendMail:"//smarket.dellemc-solution.com/cusInterapi/api/edm/send",
    // 格式化表单
    formatForm:{
        "email":"email",
        "name": "name",
        "mobile": "mobile",
        "custom_form_field_1545032510072": "solution",
        "enterprise_name": "enterprise_name",
        "industry": "industry",
        "employeesNumber":"employeesNumber",
        "custom_form_field_1545032930649":"budget",
        "custom_form_field_1545033122898": "item_des",
        "custom_form_field_1545033265212": "project_module",
        "custom_form_field_1545033342863" : "page_url"
    }
};
window.configs = {
    coreUrl: "//smarket.dellemc-solution.com/core/",
    indexUrl: "//www.smarket.net.cn/",
    seminarUrl: "//smarket.dellemc-solution.com/sde/",
    toolsUrl: "//smarket.dellemc-solution.com/tool/",
    webinarUrl: "//smarket.dellemc-solution.com/webinar/",
    webinarOpenUrl: "//smarket.dellemc-solution.com/webinaropen/",
    webinarInteractUrl: "//smarket.dellemc-solution.com/webinarinteract/",
    weChatUrl: "//smarket.dellemc-solution.com/wechat/",
    tenantUrl: "//smarket.dellemc-solution.com/tenant/",
    adminUrl: "//smarket.dellemc-solution.com/admin/",
    topicUrl: "//smarket.dellemc-solution.com/topic/",
    //閸氬骸褰撮幒銉ュ經Url
    corebUrl: "//smarket.dellemc-solution.com/coreb/",
    indexbUrl: "//smarket.dellemc-solution.com/sindexb/",
    sbaseUrl: "//smarket.dellemc-solution.com/content/",
    seminarbUrl: "//smarket.dellemc-solution.com/sdeb/",
    swcbUrl: "//smarket.dellemc-solution.com/interface/",
    templatebUrl: "//smarket.dellemc-solution.com/template/",
    toolsbUrl: "//smarket.dellemc-solution.com/toolb/",
    filebUrl: "//smarket.dellemc-solution.com/fileb/",
    webinarbUrl: "//smarket.dellemc-solution.com/webinarb/",
    webinarbInteractUrl: "//smarket.dellemc-solution.com/",
    webinarbInteractApiUrl: "//smarket.dellemc-solution.com/webinarbinteractapi/",
    adminbUrl: "//smarket.dellemc-solution.com/adminb/",
    projectUrl: "//projectsapi.smarket.net.cn/",
    topicbUrl: "//smarket.dellemc-solution.com/topicb/",
    memberb: "//smarket.dellemc-solution.com/memberb/",
    memberbUrl: "//smarket.dellemc-solution.com/memberb/",

    apiGatewayUrl: "//api.dellemc-solution.com/",
    // apiGatewayUrl: "//api-dellnew.smarket.net.cn/",
    //闁板秶鐤嗛惄绋垮彠
    cdnPrefix: "//cdn.smarket.net.cn/javascript/Saas/",
    shortUrlPrefix: "//smarket.dellemc-solution.com/u/",
    wilddogUrl: "//smarket.wilddogio.com/dell/",
    componentAppId: "wx837fd181f38f8349",
    officePrefix: "16574",
    weChatOauthUrl: "//smarket.dellemc-solution.com/memberb/public/wx_interface/auth"
};