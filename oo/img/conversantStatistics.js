(function (e) {
  var t = "775", n = document, r, i, s = {
    http: "http://cdn.mplxtms.com/s/MasterTMS.min.js",
    https: "https://secure-cdn.mplxtms.com/s/MasterTMS.min.js"
  }, o = s[/\w+/.exec(window.location.protocol)[0]];
  i = n.createElement("script"), i.type = "text/javascript", i.async = !0, i.src = o + "#" + t, r = n.getElementsByTagName("script")[0], r.parentNode.insertBefore(i, r), i.readyState ? i.onreadystatechange = function () { if (i.readyState === "loaded" || i.readyState === "complete") i.onreadystatechange = null } : i.onload = function () { try { e() } catch (t) { } }
})(function () { });
