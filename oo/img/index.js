//logo跳转
// $('.leftLogo').on("click",function(){
//     window.open('https://www.dellemc-solution.com/');
// });

//注册
function doRegister() {
    window.location.href = "https://www.dellemc-solution.com/personcenter/register.html?backUrl="+ encodeURIComponent(window.location.href);
}

//修改密码
function ModifyPassword() {
    // window.location.href = "https://www.dellemc-solution.com/personcenter/modifypassword.html?backUrl="+ encodeURIComponent(window.location.href);
    window.location.href = "/user/edit.html#changepassword";
    //判断是否处在个人信息修改页面
    if(window.cateData.pageSubModule == 'edit'){
        window.location.reload();
    }
}

//找回密码
function findPassword() {
    window.location.href = "https://www.dellemc-solution.com/personcenter/findpassword.html?backUrl="+ encodeURIComponent(window.location.href);
}

// 退出登录
function loginOut() {
  gio('clearUserId');
  smarket.api.member.logout({
    schemaId: smarket.Config("schemaId"),
    sess: smarket.cookie.smarketMember(smarket.Config('schemaId')).session,
    globalUserId: $.cookie('globalUserId')
  }).then(function (data) {
    //隐藏我的下拉框/刷新页面
    userRecode({behaviorType: '退出登录', behaviorResult: '退出登录成功',});
    $('.meCoreList').hide();
    // window.location.reload();
    //退出登录跳首页
    window.location.href = '/';
  })
}

//foter显示全部
$(".footJtD").on("click",function(){
    if($('.topInfoAll').hasClass('short')){
        $('.topInfoAll').removeClass('short');
        $('.topInfoAll').animate({height: '560px'},'slow');
    }else{
        $('.topInfoAll').addClass('short');
        $('.topInfoAll').animate({height: '80px'},'slow');
    }
    $('.icon-xiangxia2').toggle();
    $('.icon-xiangshang2').toggle();
});

//个人中心foter显示全部
$(document).on("click",".footPesonal",function(){
    if($('.topInfoAll').hasClass('short')){
        $('.topInfoAll').removeClass('short');
        $('.topInfoAll').stop().animate({height: '128px'},'fast');
    }else{
        $('.topInfoAll').addClass('short');
        $('.topInfoAll').stop().animate({height: '63px'},'fast');
    }
    $('.icon-xiangxia2').toggle();
    $('.icon-xiangshang2').toggle();
});

//点击返回顶部
$(".toTop").on("click",function(){
    $('html , body').animate({scrollTop: 0},'slow');
});

//客服
if(localStorage.getItem('isKefu')){
  localStorage.removeItem('isKefu');
}
// $('.sidebar .kefu').on("click",function(){
//     if($('.whiteKefuWra').attr("style") == "display: none;"){
//         $('.whiteKefuWra').fadeIn(400);
//     }
// });
$('.sidebar .kefu').hover(function () {
  if($('.whiteKefuWra').attr("style") == "display: none;"){
    $('.whiteKefuWra').fadeIn(400);

  }
},function () {
  var isKefu = localStorage.getItem('isKefu') ? localStorage.getItem('isKefu') : false;
  //console.log(isKefu);
  if(isKefu == false){
    $('.whiteKefuWra').fadeOut(400);
    $('.kefu').removeClass('show');
  }
});


//微信
$('.sidebar .weixin').on("click",function(){
    localStorage.removeItem('isKefu');
    $('.whiteKefuWra').hide();
    $('.kefu').removeClass('show');
    $('.whiteWechatWra').fadeIn(400);
    $(this).attr('title','');
});
$('.sidebar .weixin').hover(function () {
    // localStorage.removeItem('isKefu');
    // $('.whiteKefuWra').hide();
    // $('.kefu').removeClass('show');
    // $('.whiteWechatWra').fadeIn(400);
    $(this).attr('title','点击查看公众号二维码');
},function () {
  $('.whiteWechatWra').fadeOut(400);
});

//小程序
$('.sidebar .xcx').on("click",function(){
    localStorage.removeItem('isKefu');
    $('.whiteKefuWra').hide();
    $('.kefu').removeClass('show');
    $('.whiteXcxWra').fadeIn(400);
    $(this).attr('title','');
});
$('.sidebar .xcx').hover(function () {
    // localStorage.removeItem('isKefu');
    // $('.whiteKefuWra').hide();
    // $('.kefu').removeClass('show');
    // $('.whiteXcxWra').fadeIn(400);
    $(this).attr('title','点击查看小程序二维码');
},function () {
  $('.whiteXcxWra').fadeOut(400);
});

//客服关闭按钮
$('.icon-guanbi1').on("click",  function(){
  localStorage.removeItem('isKefu');
  $('.whiteKefuWra').fadeOut(400);
  $('.kefu').removeClass('show');
});

//返回顶部
$(document).on("click",".toTop",function(){
  $('body,html').animate({scrollTop:0},500);
  return false;
});
//返回顶部
$('.sidebar .toTop').hover(function () {
    // localStorage.removeItem('isKefu');
    // $('.whiteKefuWra').hide();
    // $('.kefu').removeClass('show');
    // $('.whiteXcxWra').fadeIn(400);
    $(this).attr('title','返回顶部');
},function () {

});

//侧边栏销售意向单跳转
$('.sales').on("click",  function(){
  fireTag();
  //注意&source=pop埋码
  window.open("https://www.dellemc-solution.com/personcenter/Sales.html?linksourcemodule="+$("#modifypassword").attr('data-module')+'&source=pop'+ "&backUrl="+ encodeURIComponent(window.location.href));
  gio('track', 'click_further_Intentions', {'consultEntrancePage': document.title, 'consultEntrancePageURL': document.URL});  //埋点实施方案
});