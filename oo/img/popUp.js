/**
 * Created by Administrator on 2018/12/14.
 */
var intDiff = parseInt(40);  //40秒弹出
var closePopCookie = "closedPopUp";
var closedPopUp = $.cookie(closePopCookie);
var popUp = function () {
    if (intDiff == 0 && closedPopUp != 1 && window.cateData.pageModule != 'detail') {
        // @do 显示弹窗
        // if($("body").width() <= 750){
        //     // $("div.popUp").show().animate({bottom:"10px"},600,'easeInOutCubic');
        // }else{
        //     // $("div.popUp").show().animate({right:"10px"},600,'easeInOutCubic');
        // }
        $('.whiteKefuWra').fadeIn(400);
        $('.kefu').addClass('show');
        localStorage.setItem('isKefu', true);
        // @do 初始化弹窗
        try {
            // 测试代码
            clearInterval(countDown);// 清除倒计时
        } catch (err) {

        }
    }
    intDiff--;
};
// var countDown = setInterval(popUp, 1000);

//文章详情页面滚动隐藏客服弹窗
$(window).scroll(function(event){
    if(window.cateData.pageModule == 'detail'){
        //$('.whiteKefuWra').fadeOut(400);
        //$('.kefu').removeClass('show');
        var storageKefuStatus = localStorage.getItem('isKefu');
        if(storageKefuStatus === 'false'){
            return;
        }
        localStorage.setItem('isKefu', false);
    }
});

//文章详情未点击关闭初始化显示客服弹窗
if(window.cateData.pageModule == 'detail' && closedPopUp != 1){
    // $('.whiteKefuWra').fadeIn(400);
    // $('.kefu').addClass('show');
    localStorage.setItem('isKefu', true);
}

if (closedPopUp == 1) {
    try {
        //clearInterval(countDown);// 清除倒计时
        intDiff = 0;
    } catch (err) {

    }
}
// @do 关闭弹窗
$("body").on("click", ".kefuInfo .sales", function () {
    // @do 跳转到表单页
    // 设置cookie,并清除弹窗倒计时
    // $.cookie(closePopCookie, 1, {expires: expireDate(), path: "/"});
    // clearInterval(countDown);
    // window.location.href = location.href = '../personcenter/Sales.html?linksourcemodule=' + $(this).parent("div.popUp").attr("data") +'&source=pop'+ '&backUrl=' + encodeURIComponent(location.href);
}).on("click", ".kefuInfo .icon-guanbi1", function () {
    // $.cookie(closePopCookie, 1, {expires: expireDate(), path: "/"});
    // clearInterval(countDown);
    // $("div.popUp").hide();
});
// 过期时间到当天24点
var expireDate = function(){
    var expire = new Date(new Date(new Date().toLocaleDateString()).getTime()+24*60*60*1000-1);
    return expire;
};




var popupnew = {
    pageStatus: {
        isOpen:false,
        isKefuAutoOpened: false,
        autoOpenTimer: null, //自动打开窗口计时器
    },
    //40秒后自动弹出
    setAtuoOpen: function () {
        var _this = this;
        var isCookieKefuOpened = $.cookie('ispopUpOpened');
        if (!isCookieKefuOpened) { //如果打开过则24小时内不再自动打开
            _this.pageStatus.autoOpenTimer = setTimeout(function () {
                //打开弹窗
                _this.open();
            },   window.cateData.kefuOpenTime *1000);
        }
    },

    //pop弹出
    open: function(){
        //打开弹窗
        if(this.pageStatus.isOpen){
            return;
        }
        $('.popUp').removeClass('hide');
        this.pageStatus.isOpen = true;
    },

    //清除计时器，如何页面不需要这个弹窗可以调用这个方法
    setDontOpen: function () {
        var _this = this;
        window.clearTimeout(_this.pageStatus.autoOpenTimer);
    },

    //设置24小时不再打开
    setPopUpCookie: function () {
        event.stopPropagation();
        $('.popUp').addClass('hide');
        this.pageStatus.isOpen = false;
        var _this = this;
        //如果窗口打开过则设置一个24小时的cookie不再触发自动打开 并取消当前页面的40秒自动打开计时器
        //清除计时器
        clearTimeout(_this.pageStatus.autoOpenTimer);
        //设置cookie
        var isCookieKefuOpened = $.cookie('ispopUpOpened');
        if(!isCookieKefuOpened){ //如果cookie不存在再设置
            var expire = new Date();
            expire.setTime(expire.getTime()+ (24 * 60 * 60 * 1000));
            $.cookie('ispopUpOpened', 'true', {expires: expire, path: '/'});
        }
    },

    jumpSales: function(){
        this.setPopUpCookie();
        gio('track', 'click_presalesConsult', {'consultEntrancePage': document.title, 'consultEntrancePageURL': document.URL});  //埋点实施方案
        window.open( "https://www.dellemc-solution.com/personcenter/Sales.html?linksourcemodule=" + window.cateData.pageModuleName + '&source=pop' + "&backUrl=" + encodeURIComponent(window.location.href));
    },

    init: function () {
        this.setAtuoOpen();
    }
};

popupnew.init();
