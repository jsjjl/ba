var pageTab = {
  //注册点击事件
  regEvent: function (callback) {
    var _this = this;
    //点击tab触发
    $('.category-list>li').on('click', function () {
      // console.log(this.classList[0])
      // tab-all
      var cateName = this.classList[0].substr(4);
      _this.changTabPage(this, cateName);
      if (callback) {
        callback(cateName)
      }
    });
  },

  //切换tab页面 obj:要切换到的tab jq元素 cateName:tab名称
  changTabPage:function(obj, cateName){
    var _this = this;
    $('.content-seld').hide();
    $('.content-' + cateName).show().addClass('content-seld');
    // initMas($('.content-' + cateName)); //初始化瀑布流
    // msLoader.setLoad(); //切换页面后设置下loader状态，避免出现已加载完毕的情况
    window.cateData.pageTab = cateName;
    // msnObj.layout(cateName);
    _this.setTab(obj); //设置选中状态
  },

  //设置选中状态
  setTab: function (ele) {
    $('.category-hover').removeClass('category-hover');
    $(ele).addClass('category-hover');
    var indicatorWidth = 15; //指示器宽度
    var indicatorOffset = 0; //指示器偏移量
    var x = $(ele).position().left; //li的位置
    var w = $(ele).outerWidth(true); //li的宽度
    indicatorWidth = w;
    indicatorOffset = x + ((w - indicatorWidth) / 2);
    $('.indicator').css({"left": indicatorOffset, "width": indicatorWidth});
  },

  init: function(callback){
    this.regEvent(callback);
    // initMasNew('.content'); //初始化瀑布流


    var seldOption = document.querySelector('.category-hover');
    if (seldOption) {
      this.setTab(seldOption);
    }
  }
};
setTimeout(function () { //延迟两秒等待dom载入完成 否则会计算出错
  pageTab.init();
},100)
