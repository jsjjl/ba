$(function(){
    //搜索功能
    $(document).click(function(e){
        var target = $(e.target);
        if(target.closest(".pcSoBox").length != 0 || target.closest(".icon-sousuo-1").length != 0) return;
        $(".pcSoBox").fadeOut(400).removeClass('on');
        $('#pcSoInp').val('');
    });
    
    var wd = $(".leftLogo a").attr("href");

    $('.icon-sousuo-1').on('click', function (e) {
        inItList();
        $('.pcSoBox').fadeIn(400).addClass('on');
        $('#pcSoInp').focus();

        
        if($('#pcSoInp').val() != '' && $('.pcSoBox').attr("style") != "display: none;"){
            window.open(wd+'?s='+encodeURI($('#pcSoInp').val()));
         /*   window.location.href = "../list/"+ $('#pcSoInp').val();*/
            // window.location.href = 'https://www.dellemc-solution.com/commonlist/search.html?keyWord=\'+$(\'#pcSoInp\').val()';
        }
    });

    //回车搜索
    $("#pcSoInp").keydown(function(event) {
        if (event.keyCode == 13 && $('#pcSoInp').val() != '') {
            //执行操作
            window.open(wd+'?s='+encodeURI($('#pcSoInp').val()));
        }
    });

    //关闭输入框
    // $('.pcSoBokBtn').on('click', function (e) {
    //     $('.pcSoBox').fadeOut(400).removeClass('on');
    //     $('.pcSoBtn').css({
    //         'background-color': 'rgba(0,0,0,0)'
    //     })
    // });

    $('.navItemList .navErBtn').hover(function () {
        $(this).find('.xiaList').stop();
        $(this).find('.xiaList').fadeIn(400);
    }, function () {
        $(this).find('.xiaList').stop();
        $(this).find('.xiaList').fadeOut(400);
    });

    // list导航按钮
    var isShowAllNav = true;
    $('.allNavBtn').on('click', function () {
        if (isShowAllNav) {
            $('.allNavBox').stop();
            $('.allNavBox').fadeIn(400);
        } else {
            $('.allNavBox').stop();
            $('.allNavBox').fadeOut(400);
            $('.allNavBtn').css({
                'background-color': 'rgba(0,0,0,0)'
            })
        }
        isShowAllNav = !isShowAllNav;
    });
    var allNavTime;
    $('.allNavBtn').hover(function () {
        inItList();
        clearTimeout(allNavTime);
        $('.allNavBox').stop();
        $('.allNavBox').fadeIn(400);
    }, function () {
        allNavTime = setTimeout(function () {
            $('.allNavBox').stop();
            $('.allNavBox').fadeOut(400);
            $('.allNavBtn').css({
                'background-color': 'rgba(0,0,0,0)'
            })
        }, 50)
    });
    $('.allNavBox').hover(function () {
        clearTimeout(allNavTime);
    }, function () {
        $('.allNavBox').stop();
        $('.allNavBox').fadeOut(400);
        $('.allNavBtn').css({
            'background-color': 'rgba(0,0,0,0)'
        })
    });
    // 个人中心--模拟cookie登录/退出
    var isShowMeCore = true;
    $('.meCore').on('click', function () {
        //判断是否登录
        var userInfo = isLogin();
        if (!userInfo) {
            if($('.meCoreList').attr("style") == "display: none;" || $('.meCoreList').is(":hidden")){
                window.location.href = "../personcenter/login.html?backUrl="+ encodeURIComponent(window.location.href);
            }else{
                $('.meCoreList').stop();
                $('.meCoreList').fadeOut(400);
            }
        }
    });

    $('.meCore').hover(function(){
        //判断是否登录
        var userInfo = isLogin();
        if (userInfo) {
            $('.meCoreList').stop();
            $('.meCoreList').fadeIn(400);
        }
    },function(){
        $('.meCoreList').stop();
        $('.meCoreList').fadeOut(400);
        $('.meCore').css({
            'background-color': 'rgba(0,0,0,0)'
        })
    });
    // 有二级导航的
    $('.navErBtn').attr('data-bok', 'ok');
    $('.navErBtn').on('click', function () {
        inItList();
        if ($(this).attr('data-bok') == 'ok') {
            $('.navErBtn .xiaList').stop();
            $('.navErBtn .xiaList').fadeOut(400);
            $('.navErBtn').attr('data-bok', 'ok');

            $(this).find('.xiaList').stop();
            $(this).find('.xiaList').fadeIn(400);
            $(this).attr('data-bok', 'no')
        } else {
            $('.navErBtn .xiaList').stop();
            $('.navErBtn .xiaList').fadeOut(400);
            $(this).attr('data-bok', 'ok')
        }
    });

    function inItList() {
        isShowAllNav = isShowProduct = isShowMeCore = true;
        $('.navErBtn').attr('data-bok', 'ok');
        $('.productListBox').stop();
        $('.meCoreList').stop();
        $('.productListBox').fadeOut(400);
        $('.meCoreList').fadeOut(400);
        $('.meCore').css({
            'background-color': 'rgba(0,0,0,0)'
        });
        $('.allNavBox').stop();
        $('.allNavBox').fadeOut(400);
        $('.allNavBtn').css({
            'background-color': 'rgba(0,0,0,0)'
        });
        $('.pcSoBtn').css({
            'background-color': 'rgba(0,0,0,0)'
        });
        $('.navErBtn .xiaList').stop();
        $('.navErBtn .xiaList').fadeOut(400);
    }

    // 底部下拉列表
    $('.allNavCont .title').attr('data-bok', 'ok');
    $('.allNavCont .title').on('click', function () {
        if (window.innerWidth < 735) {
            if ($(this).attr('data-bok') == 'ok') {
                $('.allNavCont .title').next().stop();
                $('.allNavCont .title').next().fadeOut(400);
                $('.allNavCont .title span').html('+');
                $('.allNavCont .title').attr('data-bok', 'ok');
                $(this).attr('data-bok', 'no');
                $(this).next().stop();
                $(this).next().fadeIn(400);
                $(this).find('span').html('-')
            } else {
                $(this).next().stop();
                $(this).next().fadeOut(400);
                $(this).attr('data-bok', 'ok');
                $('.allNavCont .title span').html('+')
            }
        }
    })
});

//全部导航选中效果
$('.bigNavList .felxItem').hover(function () {
    $(this).find('.ckLine').stop();
    $(this).find('.ckLine').fadeIn(400).css('display','inline-block');
}, function () {
    $(this).find('.ckLine').stop();
    $(this).find('.ckLine').fadeOut(400);
});

//三级导航
$('.nohover').hover(function () {
    $(this).find('.cpsolution').stop();
    $(this).find('.cpsolution').fadeIn(400);
}, function () {
    $(this).find('.cpsolution').stop();
    $(this).find('.cpsolution').fadeOut(400);
});