<?php get_header(); ?>

<body>
    
<?php get_sidebar('top'); ?>

    <div class="contentWrap " >
        <!--左悬浮栏开始-->
        <section class="leftWrap fl">

            <!--行业板块选择栏-->
            <div class="navWra category">

                    <ul class="blockIndexClassify category-list fl" style="margin-left:0px;">
                            <li class="tab-program category-hover">解决方案</li>
                            <li class="tab-tank"><a href="<?php echo esc_url( home_url( '/' ) ); ?>anli">案例智库</a></li>
                            <div class="indicator"></div>
                    </ul>


                <!--筛选框-->
                <ul class="screenWra">



                    <li class="sort-tab-item <?php if ( isset($_GET['order']) && ($_GET['order']=='alpha') ) echo 'seled'; ?>" bt-data="all"><a href="?order=alpha">综合</a></li>
                    <li class="sort-tab-item <?php if ( isset($_GET['order']) && ($_GET['order']=='commented') ) echo 'seled'; ?>" " bt-data="hot"><a href="?order=commented">热门</a><span class="icon iconfont icon-jiantoucx"></span></li>
                    <li class="sort-tab-item <?php if ( isset($_GET['order']) && ($_GET['order']=='date') ) echo 'seled'; ?>" " bt-data="new"><a href="?order=date">最新</a><span class="icon iconfont icon-jiantoucx"></span></li>
                    <li class="sort-filter-item shaixuan " bt-data="filter">筛选<span class="icon iconfont icon-shaixuan"></span></li>
                </ul>

                <!--底部线条-->
                <!-- <div class="nav-jt"></div> -->
                <div class="line fl" style="margin-top:0;"></div>
            </div>
         
            <!--筛选条件选择-->
            <div class="conditionsWra   ani-form-top-filter-box hide">
                <div class="screenConditions">
                    <ul class="conditionsItem">
                        <li>
                            <label class="conditionT">解决方案</label>
                            <section class="conditionItem">
                                <span filter-id="it"  data-status="off">IT转型解决方案</span>
                          
                                <span filter-id="scl"  data-status="off">生产力解决方案</span>
                              
                            </section>
                        </li>
                        <li>
                            <label class="conditionT">相关产品</label>
                            <section class="conditionItem">
                                <span filter-id="2742"  data-status="off">数据保护</span>
                               
                                <span filter-id="2743"  data-status="off">融合架构</span>
                                
                                <span filter-id="2636"  data-status="off">数据存储</span>
                              
                                <span filter-id="2635"  data-status="off">服务器</span>
                              
                                <span filter-id="2638"  data-status="off">工作站</span>
                                
                            </section>
                        </li>
                        <li>
                            <label class="conditionT">公司规模</label>
                            <section class="conditionItem">
                                <span filter-id="2718"  data-status="off">中小型企业</span>
                                
                                <span filter-id="2717"  data-status="off">大型企业</span>
                               
                            </section>
                        </li>
                        <li>
                            <label class="conditionT">应用领域</label>
                            <section class="conditionItem">
                                <span filter-id="2704"  data-status="off">备份和恢复及数据保护</span>
                               
                                <span filter-id="2710"  data-status="off">VR解决方案</span>
                                
                                <span filter-id="2713"  data-status="off">业务连续性</span>
                               
                                <span filter-id="519"  data-status="off">云计算</span>
                               
                            </section>
                        </li>
                        <li>
                            <label class="conditionT">所属行业</label>
                            <section class="conditionItem">
                                <span filter-id="2692"  data-status="off">IT服务业</span>
                              
                                <span filter-id="2691"  data-status="off">互联网相关</span>
                                
                                <span filter-id="2696"  data-status="off">媒体娱乐</span>
                              
                                <span filter-id="2641"  data-status="off">房地产</span>
                               
                                <span filter-id="2694"  data-status="off">教育</span>
                              
                            </section>
                        </li>
                    </ul>

                    <section class="conditionsBtn">
                        <em class="btnClear">清空</em>
                        <em class="btnFinish">完成<span class="icon iconfont icon-pre_up"></span></em>
                    </section>
                </div>
            </div>

            <!--筛选结果-->

            <!--瀑布流解决方案数据-->
            <div class="content-all content-seld">


                    <?php 
                        $cat_id =1; //指定分类ID

                        if (isset($_GET['order']))
                        {
                        switch ($_GET['order'])
                        {
                        case 'ID' : $orderby = 'ID'; break;
                        case 'commented' : $orderby = 'comment_count'; break;
                        case 'date' : $orderby = 'date'; break;
                        default : $orderby = 'ID';
                        }
                        }
                        $args = array('cat' => $cat_id,'orderby' => $orderby, 'order' => 'DESC','showposts' => '1000','offset' => '0');
                        query_posts( $args );
                        $b=0;		
                        if (have_posts()) :while (have_posts()) : the_post(); $b++;    
                        ?>

                        <!--item BEGIN-->
                        <a href="<?php the_permalink(); ?>" target="_blank">
                        <div class="content-item ani-form-bottom normalList">
                            <div class="imgItemWra maxH-448 j_indexp">
                            <?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'auto_img' ); } ?>
                            </div>
                            <div class="blockIntroWra">
                                <div class="blockIntro">
                                    <h2 class="blockTitle">
                                        <a href="<?php the_permalink(); ?>" target="_blank" class="normalT pad-t-0"><?php echo mb_strimwidth(get_the_title(), 0, 20, '...'); ?> </a>
                                    </h2>
                                    <p class="blockSummary"><?php echo mb_strimwidth(strip_tags(apply_filters('the_content', $post->post_content)), 0, 140,'...'); ?></p>
                                    <div class="blockBottom">
                                        <a href="" target="_blank" class="blockFrom"><?php the_author(); ?><i class="icon-gf"></i></a>
                                        <i class="bor-right"></i><em class="blockTimeNew handle-time-box" ><?php the_time('Y-n-j'); ?></em>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                        <!--item END-->

                    <?php endwhile; wp_reset_query();   else: ?><?php endif; ?>


            </div>

            <!--loading-->
            <div class="loading-box" >
                <div class="loading-infinite" >
                    <div class="loadimg" style="display: none">
                        <img src="<?php bloginfo('template_directory'); ?>/img/loading.gif" >
                    </div>
                </div>
                <div class="loading-end">已为您显示全部内容</div>
            </div>

        </section>
        <!--左悬浮栏结束-->

        <!--右悬浮栏开始-->
        <section class="rightWrap fr">


        <?php get_sidebar('right-fanan'); ?>

        <?php get_sidebar('right-huodong'); ?>

        <?php get_sidebar('footer'); ?>


        </section>
        <!--右悬浮栏结束-->
    </div>

    <!-- 加载等待 -->
    <div class="load">
        <img src="<?php bloginfo('template_directory'); ?>/img/load.gif">
    </div>
<?php get_footer(); ?>
