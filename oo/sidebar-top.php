    <!--导航栏-->
    <!-- 顶部导航开始 -->
    <header class="navBox">
        <div class="navCont clearfix">
            <!--logo图-->
            <h1 class="leftLogo fl"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">戴尔易安信</a></h1>
            <div class="rightCont fr">
                <!-- 导航 -->
               
                <!-- ico列表 -->
                <ul class="navIcoList fr">
                    <!--搜索框-->
                    <li class="pcSoBtn" style="background-color: rgba(0, 0, 0, 0);">
                        <span class="icon iconfont icon-sousuo-1"><i class="searchLine"></i></span>
                        <div class="pcSoBox">
                            <!--<div class="pcSoBokBtn"></div>-->
                            <input type="text" class="boxSize" id="pcSoInp" placeholder="搜索">
                        </div>
                    </li>


                    <!-- 个人中心 -->
                    <li class="meCore" style="background-color: rgba(0, 0, 0, 0);">


                    <?php
                    global $wpdb,$user_ID;

                    if (!$user_ID) { 

                        ?>
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>login"><span class="icon iconfont icon-gerenyonghutouxiang-1"></span></a>
                        <?php
                    }
                    else{?>
                    <span class="icon iconfont icon-gerenyonghutouxiang-1"></span>

                    <div class="boxSize xiaList meList meCoreList">
                        <div class="navErList ">
                            <!--<div class="headImg"></div>-->
                            <div class="navErItem"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">我的文章</a></div>
                            <div class="navErItem"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">我的评价</a></div>
                            <div class="navErItem"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">我的收藏</a></div>
                            <div class="navErItem"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">信息修改</a></div>
                            <div class="navErItem"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">修改密码</a></div>
                            <div class="navErItem"><a href="<?php echo wp_logout_url( home_url() ); ?>" title="Logout">退出登录</a></div>
                        </div>
                    </div>

                    <?php
                        }
                    ?>

                      


                    </li>
               
                </ul>
            </div>
        </div>
    </header>
    <!-- 顶部导航结束 -->