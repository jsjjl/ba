<?php

add_filter('use_block_editor_for_post', '__return_false');
remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );

// 图片尺寸
add_theme_support( 'post-thumbnails' );

add_image_size( 'auto_img', auto, auto, true); 

add_image_size( 'index_img', 660, 440, true);

add_image_size( 'h_auto_img', 660, auto, true);

add_image_size( 'user_list_img', 660, 370, true);



// 文字超出...
function dm_strimwidth($str ,$start , $width ,$trimmarker ){

    $output = preg_replace('/^(?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$start.'}((?:[\x00-\x7F]|[\xC0-\xFF][\x80-\xBF]+){0,'.$width.'}).*/s','\1',$str);

    return $output.$trimmarker;

}


// 分类名称
function get_post_category_id($post_ID){

	global $wpdb;

	$sql="SELECT `term_taxonomy_id` FROM $wpdb->term_relationships WHERE `object_id`='".$post_ID."';";

	$cat_id=$wpdb->get_results($sql); 

	foreach($cat_id as $catId){

		$output=$catId->term_taxonomy_id;

	}

	$myCatId=intval($output);

	$term = get_term( $myCatId, 'taxonomy_name' );

	echo $term->name;

}




// 分页
function pagination($query_string){   

global $posts_per_page, $paged;   

$my_query = new WP_Query($query_string ."&posts_per_page=-1");   

$total_posts = $my_query->post_count;   

if(empty($paged))$paged = 1;   

$prev = $paged - 1;   

$next = $paged + 1;   

$range = 3; // only edit this if you want to show more page-links   

$showitems = ($range * 2)+1;   

  

$pages = ceil($total_posts/$posts_per_page);

$total_posts = $my_query->post_count; 


if($paged < $pages){ 

echo ( "<a href='".get_pagenum_link($next)."' class='nextPage'>加载更多文章</a>");  

}  


}  



// 谷歌字体
function ashuwp_remove_open_sans() {  

    wp_deregister_style( 'open-sans' );

    wp_register_style( 'open-sans', false ); 

    wp_enqueue_style('open-sans','');

}  

add_action('init','ashuwp_remove_open_sans');

// 支持中文用户名
function ludou_non_strict_login( $username, $raw_username, $strict ) {
    if( !$strict )
        return $username;

    return sanitize_user(stripslashes($raw_username), false);
}
add_filter('sanitize_user', 'ludou_non_strict_login', 10, 3);