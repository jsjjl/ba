<?php get_header(); ?>

<?php get_sidebar('top'); ?>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/articleDetail.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/xx.css">

<div class="contentWrap clearfix" style="">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <!--左悬浮栏开始-->
            <section class="leftWrap fl leftWrap_article">
                
                <div class="top_detail">
                    <div class="location">
                        <span class="iconfont icon-zuobiao-1 x"></span>
                        <span class="txt"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="txt">首页</a></span>
                        <span class="iconfont icon-right arrow"></span>
                        <!--文件中心面包屑-->
                        <span class="txt">正文</span>
                    </div>
                    <div class="top_title">
                        <div class="time fl">
                            <p class="year"><span class="iconfont icon-jianhao x"></span><?php the_time('Y'); ?><span class="iconfont icon-jianhao x"></span></p>
                            <p class="date"><?php the_time('n/j'); ?></p>
                           
                        </div>
                        <div class="article_tit fl">
                            <h1 class="tit_one"><?php the_title(); ?></h1>
                            <div class="tit_sec">
                            <?php the_tags('  ' , ''); ?>
                                <div class="fr user_operate">
                                    <em>咨询</em>
                                    <em>收藏</em>
                                </div>
                            </div>
                        </div>
        
                    </div>
                </div>
        
                      
                    
                <div class="article_txt">
                <?php the_content(""); ?>
                </div>




                <div class="hot_comments">
                    <div class="com_tit_box">
                        <div class="comment_tit">热门评论</div>
                    </div>
                    <div class="main">
                        <div class="publish_com Input_Box">
                            <div contenteditable="true" class="Input_text div"></div>
                            <p class="tips" >
                                <a href="login.html">登录</a>
                                <span>后发表评论</span>
                            </p>
                           
                            <div class="panel panel-default">
                                    <div class="panel-heading">方案能力&nbsp;：</div>
                                    <div class="panel-body">
                                                    <input name="starNum" class="starNum" type="hidden"/>
                                                <div class="starability-container">
                                                     <fieldset class="starability-slot">
                                                          <input type="radio" id="rate_1_0_1_51" name="starLevel" value="5" /> 
                                                          <label for="rate_1_0_1_51" title="5星"></label>
                                                          <input type="radio" id="rate_1_0_1_41" name="starLevel" value="4" /> 
                                                          <label for="rate_1_0_1_41" title="4星"></label>
                                                          <input type="radio" id="rate_1_0_1_31" name="starLevel" value="3" /> 
                                                          <label for="rate_1_0_1_31" title="3星"></label>
                                                          <input type="radio" id="rate_1_0_1_21" name="starLevel" value="2" /> 
                                                          <label for="rate_1_0_1_21" title="2星"></label>
                                                          <input type="radio" id="rate_1_0_1_11" name="starLevel" value="1" /> 
                                                          <label for="rate_1_0_1_11" title="1星"></label>
                                                     </fieldset>
                                                </div>
                                                <span style="font-size: 2rem;line-height: 2.9rem;padding: 1rem;font-style: oblique;" id="starStr"></span>
                                    </div>
                            </div>

                            <div class="panel panel-default">
                                    <div class="panel-heading">客户服务&nbsp;：</div>
                                    <div class="panel-body">
                                                    <input name="starNum" class="starNum" type="hidden"/>
                                                <div class="starability-container">
                                                     <fieldset class="starability-slot">
                                                          <input type="radio" id="rate_1_0_1_52" name="starLevel" value="5" /> 
                                                          <label for="rate_1_0_1_52" title="5星"></label>
                                                          <input type="radio" id="rate_1_0_1_42" name="starLevel" value="4" /> 
                                                          <label for="rate_1_0_1_42" title="4星"></label>
                                                          <input type="radio" id="rate_1_0_1_32" name="starLevel" value="3" /> 
                                                          <label for="rate_1_0_1_32" title="3星"></label>
                                                          <input type="radio" id="rate_1_0_1_22" name="starLevel" value="2" /> 
                                                          <label for="rate_1_0_1_22" title="2星"></label>
                                                          <input type="radio" id="rate_1_0_1_12" name="starLevel" value="1" /> 
                                                          <label for="rate_1_0_1_12" title="1星"></label>
                                                     </fieldset>
                                                </div>
                                                <span style="font-size: 2rem;line-height: 2.9rem;padding: 1rem;font-style: oblique;" id="starStr"></span>
                                    </div>
                            </div>

                            <div class="panel panel-default">
                                    <div class="panel-heading">产品功能&nbsp;：</div>
                                    <div class="panel-body">
                                                    <input name="starNum" class="starNum" type="hidden"/>
                                                <div class="starability-container">
                                                     <fieldset class="starability-slot">
                                                          <input type="radio" id="rate_1_0_1_53" name="starLevel" value="5" /> 
                                                          <label for="rate_1_0_1_53" title="5星"></label>
                                                          <input type="radio" id="rate_1_0_1_43" name="starLevel" value="4" /> 
                                                          <label for="rate_1_0_1_43" title="4星"></label>
                                                          <input type="radio" id="rate_1_0_1_33" name="starLevel" value="3" /> 
                                                          <label for="rate_1_0_1_33" title="3星"></label>
                                                          <input type="radio" id="rate_1_0_1_23" name="starLevel" value="2" /> 
                                                          <label for="rate_1_0_1_23" title="2星"></label>
                                                          <input type="radio" id="rate_1_0_1_13" name="starLevel" value="1" /> 
                                                          <label for="rate_1_0_1_13" title="1星"></label>
                                                     </fieldset>
                                                </div>
                                                <span style="font-size: 2rem;line-height: 2.9rem;padding: 1rem;font-style: oblique;" id="starStr"></span>
                                    </div>
                            </div>

                            <div class="panel panel-default">
                                    <div class="panel-heading">&nbsp;&nbsp;&nbsp;&nbsp;性价比：</div>
                                    <div class="panel-body">
                                                    <input name="starNum" class="starNum" type="hidden"/>
                                                <div class="starability-container">
                                                     <fieldset class="starability-slot">
                                                          <input type="radio" id="rate_1_0_1_54" name="starLevel" value="5" /> 
                                                          <label for="rate_1_0_1_54" title="5星"></label>
                                                          <input type="radio" id="rate_1_0_1_44" name="starLevel" value="4" /> 
                                                          <label for="rate_1_0_1_44" title="4星"></label>
                                                          <input type="radio" id="rate_1_0_1_34" name="starLevel" value="3" /> 
                                                          <label for="rate_1_0_1_34" title="3星"></label>
                                                          <input type="radio" id="rate_1_0_1_24" name="starLevel" value="2" /> 
                                                          <label for="rate_1_0_1_24" title="2星"></label>
                                                          <input type="radio" id="rate_1_0_1_14" name="starLevel" value="1" /> 
                                                          <label for="rate_1_0_1_14" title="1星"></label>
                                                     </fieldset>
                                                </div>
                                                <span style="font-size: 2rem;line-height: 2.9rem;padding: 1rem;font-style: oblique;" id="starStr"></span>
                                    </div>
                            </div>

                            <div class="panel panel-default">
                                    <div class="panel-heading">&nbsp;&nbsp;&nbsp;&nbsp;稳定性：</div>
                                    <div class="panel-body">
                                                    <input name="starNum" class="starNum" type="hidden"/>
                                                <div class="starability-container">
                                                     <fieldset class="starability-slot">
                                                          <input type="radio" id="rate_1_0_1_55" name="starLevel" value="5" /> 
                                                          <label for="rate_1_0_1_55" title="5星"></label>
                                                          <input type="radio" id="rate_1_0_1_45" name="starLevel" value="4" /> 
                                                          <label for="rate_1_0_1_45" title="4星"></label>
                                                          <input type="radio" id="rate_1_0_1_35" name="starLevel" value="3" /> 
                                                          <label for="rate_1_0_1_35" title="3星"></label>
                                                          <input type="radio" id="rate_1_0_1_25" name="starLevel" value="2" /> 
                                                          <label for="rate_1_0_1_25" title="2星"></label>
                                                          <input type="radio" id="rate_1_0_1_15" name="starLevel" value="1" /> 
                                                          <label for="rate_1_0_1_15" title="1星"></label>
                                                     </fieldset>
                                                </div>
                                                <span style="font-size: 2rem;line-height: 2.9rem;padding: 1rem;font-style: oblique;" id="starStr"></span>
                                    </div>
                            </div>

       
                          
                            <div class="publish_btn postBtn" onclick="postComments(this)">发布</div>
                        </div>
                        <div class="faceDiv faceDiv_mt" style="display: none;">
                            <section class="emoji_container">
                            </section>
                            <section class="emoji_tab" style="display:none"></section>
                        </div>
                    </div>
        
                </div>


                <div class="comments_box"><div class="comment_list ppp" id="com_300444" data-child-comment-num="0" data-id="314038" data-collect="0" data-negative="0"><img src="<?php bloginfo('template_directory'); ?>/img/314038.jpg" class="photo fl"><div class="com_content fl"><div class="com_top"><div class="com_user fl"><span class="user_name">一**柔病</span><span class="com_time">2019-04-25 16:40</span></div><div class="com_operate fr"><span class="huifu_parent"><span class="iconfont icon-comment huifu_default"></span><span class="iconfont icon-liuyan-1 huifu_col hide"></span><span class="txt_cen fff">回复</span><i class="right_m txt_cen">0</i></span><span class="good_default_fu"><span class="iconfont icon-appreciate2 good_default xxx" data-type="1"></span><span class="iconfont icon-appreciate1 good_col hide xxx"></span><i class="right_m txt_cen">0</i></span><span class="iconfont icon-appreciate-copy cai_default xxx"></span><span class="iconfont icon-appreciate-1-copy cai_col hide xxx"></span></div></div><div class="com_bottom"><p>太专业了 好多专业名词都不知道啥意思</p></div></div><div class="clearfix"></div><div class="clearfix main_300444"><div class="huifu_kuang huifu_kuang_w1 hide fr Input_Box ani-form-top-filter-box"><div contenteditable="true" class="Input_text divv" onkeyup="comments_numbers(this)"></div><div class="number_box_huifu zi_fu"><span>还可以输入</span><i class="zi_num">500</i><span>个字</span></div><div class="expression_huifu Input_Foot"><span class="imgBtn"><span class="iconfont icon-biaoqing"></span><span>表情</span></span></div><div class="huifu_btn fr" onclick="postComments(this)">发布</div></div><div class="faceDiv fl mr_left faceDiv_mt_15" style="display: none;"><section class="emoji_container"></section><section class="emoji_tab" style="display:none"></section></div></div></div></div>



                <!--文章点赞状态-->
                <input type="hidden" value="0" id="collectStatus">
                <!--文章差评状态-->
                <input type="hidden" value="0" id="negativeStatus">
                <div class="clearfix"></div>
                <div class="more" data-bind="visible:isLoading" style="display: none;">
                    <div id="ddr">
                        <div class="ddr ddr1"></div>
                        <div class="ddr ddr2"></div>
                        <div class="ddr ddr3"></div>
                    </div>
                </div>
                <div class="loading-end" data-bind="visible:(isLastPage()&amp;&amp;totalComments()>0)" style="">
                    已为您显示全部评论
                </div>
        
                <!--linksourcemodule参数-->
                <span id="moduleType" data-module="IT转型解决方案" class="hide"></span>
            
            </section>
            <!--左悬浮栏结束-->
        



            <!--右悬浮栏开始-->
            <section class="rightWrap fr">
             
                    <div class="user_top">
                            <?php echo get_avatar( get_the_author_email(), '60' );?>
                        <div class="user_tit">
                            <p class="user_level_one">
                                        <a href="#" target="_blank"><?php the_author(); ?></a>
                                            <img src="<?php bloginfo('template_directory'); ?>/img/vip_icon.png" class="vip_icon">
                            </p>
                            <p class="user_level_sec hide"></p>
                        </div>
                    <div class="jjl_more_l">
                        <p><em><?php echo count_user_posts(get_the_author_id(), 'post', true);?></em><span>解决方案</span></p>
                        <p><em><?php echo count_user_posts(get_the_author_id(), 'post', true);?></em><span>案例快讯</span></p>
                    </div>

                <!--案例智库-->
                <div class="mar-t-47">
                    <div class="botLine">最近方案</div>
                    
                    <ul class="articlesItem">

                    <?php 
                        $cat_id =1; //指定分类ID
                        $orderby = 'date';
                        $args = array('cat' => $cat_id,'orderby' => $orderby, 'order' => 'DESC', 'author' =>get_the_author(),'showposts' => '8');
                        query_posts( $args );
                        $b=0;		
                        if (have_posts()) :while (have_posts()) : the_post(); $b++;    
                        ?>

                            <li><a href="<?php the_permalink(); ?>" target="_blank" class="blockFrom"><?php echo mb_strimwidth(get_the_title(), 0, 20, '...'); ?></a></li>

                    <?php endwhile; wp_reset_query();   else: ?><?php endif; ?>
                    </ul>
                </div>

         <!--案例智库-->
         <div class="mar-t-47">
                <div class="botLine">合作客户</div>
                
                <?php if ( get_post_meta($post->ID, '合作客户', true) ) {?>
                    <div class="hezuo hide"><?php echo get_post_meta($post->ID, '合作客户',true);?></div>
                <?php }?>
                <ul class="articlesItem hezuolist">
                   
                </ul>
            </div>

      
            <?php endwhile; else: ?><?php endif; ?>
                
                </div>
               
                <!--案例智库-->
            <div class="mar-t-30">

                <div class="botLine">相关解决方案</div>
                
                <ul class="articlesItem">

                <?php
                $category = get_the_category();
                $category_id=$category[0]->cat_ID;
                query_posts('cat='.$category_id.'&showposts=5');
                ?>
                <ul class="related_post">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <li><a href="<?php the_permalink(); ?>" target="_blank" class="blockFrom"><?php the_title(); ?></a></li>

                <?php endwhile;?>
                <?php endif; ?>

                </ul>
            </div>


            
			<?php get_sidebar('footer'); ?>
            </section>
            <!--右悬浮栏结束-->
        </div>


    <?php get_footer(); ?>

    <script>
    $(function(){
        $('[name="starLevel"]').bind("click",function(){
        
            console.log($(this)[0].value);
            
            $(this).parent().parent().prev().val($(this)[0].value);
            var starNum = $(this)[0].value;

        })

        $(".tit_sec a").attr("href","#");



        var arr=new Array();
        var henzuohtml = "";
        var str = $(".hezuo").html();
    
        arr=str.split(',');
        for(var i=0;i<arr.length;i++)
        {
            henzuohtml +=`<li>${arr[i]}</li>`;
        }
        console.log(henzuohtml);
        $(".hezuolist").html(henzuohtml);

    
    })
    </script>