<?php
/*
Template Name:
*/
?>
<?php get_header(); ?>             

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    	<div class="page-template-wrapper">
            	<div class="page-template-content page-content full-width-page full-width-page-content clearfix container_8">
                     <style type="text/css">
                     .wzblog-content img{ display:none !important;}
					 .tpblog-content p{ text-indent:-9999px; 
 white-space:nowrap; 
							 line-height:0.1; color:#fff;}
						 .tpblog-content p a{ text-indent:-9999px; 
 white-space:nowrap; 
							 line-height:0.1; color:#fff;}
					 .tpblog-content img{ display:block !important;}
					 .tpblog-content p img{ display:block !important;}
                     </style>
                 <div class="grid_4 or3of8">
					<div class="container">		
					<div class="post type-post status-publish format-standard hentry category-blog blog-post clearfix">
						<div class="clearfix">	
							<div class="blog-text">	
								  
							<style>.blog-entry p{margin-bottom: 16px !important; line-height: 145% !important;}</style>
								<div class="blog-entry">
									<div class="blog-content wzblog-content">
										<p><strong>


北京博达云创投资管理有限公司

</strong><br></p>
                                        <div class="j_hr" style=" width:100%; height:1px; border-top:1px solid #33cccc;margin-bottom: 16px !important;"></div>
<p>
关于我们
<br><br>
Dooo Design Studio 是一个多元的艺术设计机构，工作包括品牌形象设计、美术指导、广告传播、动态媒体与平面印刷。我们认为创意的火花没有大小之分，我们会捕捉任何一个灵感并发展出有力的设计概念。进行创造性的工作是我们的原动力，当然最重要的是我们辛勤的工作切实满足了客户的需求。

</p>
<p><strong></strong></p>
<p>

We are a multi-disciplinary graphic design studio whose work covers brand identity and development, art direction, advertising, motion and printed literature. We believe that design should stem from good ideas no matter how big or small. In our studio we look to create work that excites, inspires and most importantly meets the need of client.

</p>
<p>&nbsp;</p>
										<p>&nbsp;</p>
										
								
										
										<br>		<div class="share-it">
										<div class="share-fix">	
											   </div>  </div>                     
										<br>
										
       
                    
									</div>
									<div class="pagelink">
									       
										
									</div>
									   
									<style>.page-content {
padding-left: 10px;
padding-right: 10px;
font-size: 16px !important;
line-height: 145% !important;
}
									
									.awards ul li {
list-style-type: none;
padding: 0;
margin: 0;
font-size: 16px !important;
line-height:  145% !important;
}
									.blog-entry p {
margin-bottom: 16px !important;
line-height: 145% !important;
}
.blog-entry p{  float: none;}										
									</style>

									<img width="830" height="569" src="

http://www.dooodesign.com/wp-content/uploads/2014/10/01-900x563.jpg
" alt="donkey" style="margin-top:-15%;"> 
									
									<br clear="all">
<br>
<!-- bottom services -->

<div class=" dude lft" style="margin-top: 2em;">
	<h5 class="smallbold">

服务

</h5>
     <div class="j_hr" style=" width:100%; height:1px; border-top:1px solid #33cccc;margin-bottom: 16px !important;"></div>
<p> <strong>

品牌 Branding

</strong> <br>

策略 Strategy 

<br>

标志 Logo

<br>

形象 VI

<br> </p>  <p> <strong>

印刷 Print

</strong> <br>

书刊 Book 

<br>

海报 Poster 

<br>

纸品 Paper

</p>  <p> <strong>

线上 Online

</strong> <br>

应用 APP  

<br>

网站 Web 

<br>
微博 Weibo

<br>
微信 Wechat

<br>
</p>
</div>
<br clear="all">
<br>
									
									
<!-- bottom services -->
									
									
		 
</div><!-- blog entry -->
</div><!-- blog text -->
							
							
							<!-- where blog meta was.. -->
						</div><!-- blog inside -->
					</div><!-- blog post -->
                    </div>
                    </div>
                 <div class="newthirds">
                 
					 
					<div class="mobile-pad">


						<div class="">
<!-- bottom services -->

<div class=" dude lft">
	<h5 class="smallbold">

联系

</h5>
	<div class="j_hr" style=" width:100%; height:1px; border-top:1px solid #33cccc;margin-bottom: 16px !important;"></div>
<ul>



<li>
霍文杰
<br>
<br>- &nbsp;
联合创始人
<br>- &nbsp;
品牌 / 网站
<br>- &nbsp;
13810447650
<br>- &nbsp;
175466429@qq.com
</li>

<br>

<li>
白凤鹍
<br>
<br>- &nbsp;
联合创始人
<br>- &nbsp;
品牌 / 书刊
<br>- &nbsp;
18210383698
<br>- &nbsp;
250382058@qq.com
</li>

<br>

<br>

<li>
李刚
<br>
<br>- &nbsp;
摄影 / 造型
<br>- &nbsp;
15672301965
<br>- &nbsp;
150180350@qq.com
</li>

<br>

<li>
崔俊锋
<br>
<br>- &nbsp;
空间 / 导视
<br>- &nbsp;
18810918332
<br>- &nbsp;
409564900@qq.com
</li>

<br>



</ul>
</div>


<div class=" dude awards">
	<h5 class="smallbold">

荣誉
</h5>
<div class="j_hr" style=" width:100%; height:1px; border-top:1px solid #33cccc;margin-bottom: 16px !important;"></div><ul>



<li> <strong> 
- 2019 - 奖项 / 展览
</strong> </li>

<br>

<li>
• [ 美国 ] 第98届纽约ADC年度设计大奖
</li>

<li>
• [ 英国 ] D&AD 设计大奖 2019
</li>

<li>
• [ 塞浦路斯 ] 塞浦路斯国际海报三年展 2019
</li>

<li>
• [ 塞浦路斯 ] Graphic Stories Cyprus 国际海报竞赛
</li>

<li>
• [ 波兰 ] Roller Poster 2019 国际海报展
</li>

<li>
• [ 波兰 ] Stop Hate 国际海报展
</li>

<li>
• [ 伊朗 ] Posterrorism 国际海报竞赛 2019
</li>

<li>
• [ 西班牙 ] 城市字体海报展：纽约
</li>

<li>
• [ 西班牙 ] 城市字体海报展：洛杉矶
</li>

<li>
• [ 西班牙 ] 城市字体海报展：芝加哥
</li>

<li>
• [ 西班牙 ] 城市字体海报展：迈阿密
</li>

<li>
• [ 西班牙 ] 城市字体海报展：马赛
</li>

<li>
• [ 西班牙 ] 城市字体海报展：热那亚
</li>

<li>
• [ 西班牙 ] 城市字体海报展：内罗毕
</li>

<li>
• [ 大陆 ] 京津冀设计名家邀请展
</li>


<br>


<li>
------------------------------
</li>

<br>


<li> <strong> 
- 2018 - 奖项 / 展览
</strong> </li>

<br>

<li>
• [ 日本 ] 富山国际海报三年展 2018
</li>

<li>
• [ 俄罗斯 ] 金蜜蜂国际平面设计双年展 13
</li>

<li>
• [ 伊朗 ] 恐怖主义的终结 国际海报大赛
</li>

<li>
• [ 西班牙 ] 城市字体海报展：洛杉矶
</li>

<li>
• [ 西班牙 ] 城市字体海报展：威尼斯
</li>

<li>
• [ 西班牙 ] 城市字体海报展：那不勒斯
</li>

<li>
• [ 台湾 ] 台北设计奖 2018
</li>

<li>
• [ 台湾 ] 金点设计奖 2018
</li>

<li>
 • [ 大陆 ] 中国最美的书 2018
</li>

<li>
• [ 大陆 ] 贵姓 全球华人姓氏文化汉字设计展
</li>

<li>
• [ 大陆 ] 透明海洋 主题海报邀请展
</li>

<br>


<li> <strong> 
- 2018 - 出版
</strong> </li>  

<br>

<li>
• [ 大陆 ] Brand 创意呈现 Ⅳ
</li>

<li>
• [ 大陆 ] 字体呈现 2017
</li>

<li>
• [ 大陆 ] ekoo 300
</li>



<br>

<li>
------------------------------
</li>

<br>

<li> <strong> 
- 2017 - 奖项 / 展览
</strong> </li>

<br>

<li>
 • [ 芬兰 ] 拉赫蒂国际海报三年展 2017 
</li>  

<li>
 • [ 波兰 ] 卢布林国际海报双年展 第三届
</li>  

<li>
 • [ 美国 ] Poster on Politics 海报展 
</li> 

<li>
 • [ 法国 ] Poster for tomorrow 国际海报竞赛 
</li>  

<li>
 • [ 玻利维亚 ] 玻利维亚国际海报双年展 2017
</li>

<li>
 • [ 斯洛伐克 ] 巴尔代约夫海报四年展 2017
</li>

<li>
 • [ 斯洛伐克 ] 国际生态海报三年展 2017
</li>

<li>
 • [ 委内瑞拉 ] No Barriers 国际海报竞赛
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：迪拜
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：布宜诺斯艾利斯
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：首尔
</li>

<li>
 • [ 澳门 ] 澳门设计双年展 11
</li>

<li>
 • [ 香港 ] 国际标志设计奖 CGDA 2017
</li>

<li>
 • [ 大陆 ] 中国最美的书 2017
</li>

<li>
 • [ 大陆 ] 上海亚洲平面设计双年展 第四届
</li>

<li>
 • [ 大陆 ] China TDC 文字设计在中国邀请展
</li>

<li>
 • [ 大陆 ] 国际视觉赛事中国设计师优秀作品展
</li>

<li>
 • [ 大陆 ] 鸡密档案 报晓设计邀请展
</li>

<li>
 • [ 大陆 ] Hiiibrand Awards 2017
</li>

<br>

<li> <strong> 
- 2017 - 出版
</strong> </li>  

<br>

<li>
• [ 香港 ] APD 13 亚太设计年鉴
</li>


<br>


<li>
------------------------------
</li>

<br>

<li> <strong> 
- 2016 - 奖项 / 展览
</strong> </li>  

<br>

<li>
 • [ 墨西哥 ] 墨西哥国际海报双年展 14届
</li>

<li>
 • [ 比利时 ] 蒙斯国际政治海报三年展 13届
</li>

<li>
 • [ 意大利 ] Cheep 街头海报艺术节
</li>

<li>
 • [ 波兰 ] Art Moves 广告牌艺术节
</li>

<li>
 • [ 捷克 ] 布拉格虚拟双年展 2016
</li>

<li>
 • [ 波兰 ] 中波视觉设计展 2016
</li>

<li>
 • [ 韩国 ] New Vision 海报韩国展
</li>

<li>
 • [ 马其顿 ] 红十字会海报设计竞赛
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：巴黎
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：波尔图
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：柏林
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：马拉喀什
</li>

<li>
 • [ 台湾 ] 汉字设计双年展 2016
</li>

<li>
 • [ 大陆 ] 中国最美的书 2016
</li>

<li>
 • [ 大陆 ] 深圳国际海报节 2016
</li>

<li>
 • [ 大陆 ] Hiiibrand 2015 品牌标志设计大赛
</li>

<li>
 • [ 大陆 ] 全国书籍设计艺术展 深圳站
</li>

<li>
 • [ 大陆 ] 艺酷创意设计猴年微展览
</li>

<br>

<li> <strong> 
- 2016 - 出版
</strong> </li>  

<br>

<li>
 • [ 大陆 ] 品牌设计零距离
</li>

<li>
 • [ 大陆 ] 字体呈现 2016
</li>

<li>
 • [ 香港 ] APD 12 亚太设计年鉴
</li>

<br>

<li>
------------------------------
</li>

<br>

<li> <strong> 
- 2015 - 奖项 / 展览
</strong> </li>  

<br>

<li>
 • [ 斯洛伐克 ] 特纳瓦国际海报三年展 2015
</li>

<li>
 • [ 乌克兰 ] Stop Censorship 海报展
</li>

<li>
 • [ 捷克 ] 布拉格虚拟双年展 2015
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：马德里
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：雷克雅未克
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：哈瓦那
</li>

<li>
 • [ 台湾 ] 台湾国际平面设计奖 2015
</li>

<li>
 • [ 台湾 ] CTA: 亚洲创意海报设计展
</li>

<li>
 • [ 台湾 ] New Vision 亚太杰出设计海报设计展
</li>

<li>
 • [ 大陆 ] GDC 15 设计奖
</li>

<li>
 • [ 大陆 ] 靳埭强设计奖 2015
</li>

<li>
 • [ 大陆 ] 上海亚洲平面设计双年展 第三届
</li>

<li>
 • [ 大陆 ] 北京国际设计周 城市国际公益海报展
</li>

<li>
 • [ 大陆 ] 创意 x 社会  同志主题创意大赛
</li>

<li>
 • [ 大陆 ] 融 2015国际字体创意设计大赛
</li>

<li>
 • [ 大陆 ] Hiii Photography 2015 摄影大赛
</li>

<br>

<li> <strong> 
- 2015 - 出版
</strong> </li>  

<br>

<li>
 • [ 台湾 ] CTA: 亚洲创意对话
</li>

<li>
 • [ 香港 ] Design 360° Vol.60
</li>

<li>
 • [ 大陆 ] Good Idea 4
</li>

<li>
 • [ 香港 ] APD 11 亚太设计年鉴
</li>

<li>
 • [ 大陆 ] 中国设计年鉴 9
</li>

<br>

<li>
------------------------------
</li>

<br>

<li> <strong> 
- 2014 - 奖项 / 展览
</strong> </li>
<br>

<li>
 • [ 芬兰 ] 拉赫蒂国际海报三年展 2014 
</li>  

<li>
 • [ 塞尔维亚 ] 国际大学生海报双年展 2014 
</li>  

<li>
 • [ 西班牙 ] 城市字体海报展：OFFF 2014
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：孟买
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：伊萨卡岛
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：拉斯维加斯
</li>

<li> 
• [ 香港 ] 香港国际海报三年展 2014 
</li>  

<li> 
• [ 澳门 ] 澳门回归15周年海报展 
</li>  

<li> 
• [ 台湾 ] 杰出华文设计作品展 2014
</li>  

<li> 
• [ 大陆 ] 靳埭强设计奖 2014 
</li>  

<li> 
• [ 大陆 ] Hiiibrand 2013 品牌标志设计大赛 
</li>   

<li> 
• [ 大陆 ] 方正奖中文字体设计大赛 第七届 
</li>  

<br>

<li> <strong> 
- 2014 - 出版
</strong> </li>
<br>

<li> 
• [ 香港 ]  APD 10   亚太设计年鉴
</li>

<li> 
• [ 大陆 ] 国际设计年鉴 2014 
</li>  

<li>
 • [ 大陆 ] 中国设计年鉴 9 
</li>  

<br>

<li>
------------------------------
</li>

<br>

<li> <strong> 
- 2013 - 奖项 / 展览
</strong> </li>
<br>  

<li>
 • [ 英国 ] 城市字体海报展：伦敦
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：东京
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：伦敦
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：OFFF 2013
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：伊斯坦布尔
</li>

<li>
 • [ 西班牙 ] 城市字体海报展：里约热内卢
</li>

<li>
• [ 澳门 ] 澳门设计双年展 第九届 
</li>  

<li> 
• [ 大陆 ] 全国书籍设计艺术展览 第八届
</li>  

<li> 
• [ 大陆 ] 国际商标标志双年奖 第八届 
</li>  

<li> 
• [ 大陆 ]  GDC 13 设计奖 
</li>  

<br>  

<li> <strong> 
- 2013 - 出版
</strong> </li>

<br>  

<li> 
• [ 香港 ]  APD 9  亚太设计年鉴
</li>  




<br>

<li>
------------------------------
</li>

<br>

<li> <strong> 
- 2012 - 奖项 / 展览
</strong> </li>
<br>  

<li> 
• [ 丹麦 ] 奥胡斯国际海报展 2012 
</li>  

<li> 
• [ 韩国 ] 釜山国际广告节 AD STARS 2012 
</li>  

<li> 
• [ 大陆 ] 靳埭强设计奖 2012 
</li>  

<li> 
• [ 大陆 ] 中国设计大展 2012 
</li>  

<li> 

• [ 大陆 ] 微时代全球字体设计展 
</li>  

<li> 
• [ 大陆 ] Hiii Photography 2012 摄影大赛 
</li>

<br>  
<li> <strong> 
- 2012 - 出版
</strong> </li>
<br>  

<li> 
• [ 香港 ]  APD 8  亚太设计年鉴
</li>





<br>

<li>
------------------------------
</li>

<br>

 
<li> <strong> 
- 2011 - 奖项 / 展览
</strong> </li>
<br>  

<li> 
• [ 大陆 ] GDC 11 设计奖
</li>  

<li> 
• [ 大陆 ] Hiiibrand 2011 国际品牌标志大赛
</li>  

<br>  
<li> <strong> 
- 2011 - 出版
</strong> </li>
<br>  

<li> 
• [ 美国 ] Logo Lounge 6
</li>  

<li> 
• [ 大陆 ] 中国设计师作品年鉴 2011 
</li>  

<li> 
• [ 大陆 ] 新平面 Vol.28
</li>  



<br>

<li>
------------------------------
</li>

<br>
 
<li> <strong> 
- 2010 - 奖项 / 展览
</strong> </li>
<br>  

<li> 
• [ 俄罗斯 ] Best of Best品牌设计大赛2010 
</li>  

<li> 
• [ 大陆 ] Hiiibrand Awards国际品牌标志大赛 2010  
</li>  

<li> 
• [ 大陆 ] 利奥杯包装与印刷设计大赛 第三届  
</li>  

<br>  
<li> <strong> 
- 2010 - 出版
</strong> </li>
<br>  

<li> 
• [ 大陆 ] Brand! Vol.3
</li>  


<br>

<li>
------------------------------
</li>

<br>
<li> <strong> 
- 2009 - 奖项 / 展览
</strong> </li>
<br>  

<li> 
• [ 台湾 ] 台湾国际平面设计奖 2009  
</li>  <li> 

• [ 大陆 ] GDC 09 平面设计在中国 09  
</li>  

<li> 
• [ 大陆 ] 靳埭强设计奖 2009  
</li>

<li> 
• [ 大陆 ] 中南星奖 艺术设计大赛13 
</li>

<li> 
• [ 大陆 ] 广东之星 艺术设计大赛 2009
</li>



<br>

<li>
------------------------------
</li>

<br>

<li> <strong> 
- 2008 - 奖项 / 展览
</strong> </li>
<br>  



<li> 
• [ 俄罗斯 ] Best of Best品牌设计大赛 2008 
</li>  

<li> 
• [ 澳门 ] 澳门设计双年展 第七届 
</li>  

<li> 
• [ 大陆 ] 白金创意平面设计大赛 第九届 
</li>  

<li> 
• [ 大陆 ] 深圳公益广告大赛 第三届 
</li>  

<li> 
• [ 大陆 ] 靳埭强设计奖 2008  
</li>  

<li> 
• [ 大陆 ] ADI 设计奖 Vol.2  
</li>  

<li> 
• [ 大陆 ] 东+西 国际海报双年展 第二届 
</li>  

<li> 
• [ 大陆 ] 中国之星设计艺术大奖 
</li>




<br>

<li>
------------------------------
</li>

<br>
  
<li> <strong> 
- 2007 - 奖项 / 展览
</strong> </li>
<br>  


<li> 
• [ 大陆 ] ADI 设计奖 Vol.1 
</li>  

<li> 
• [ 大陆 ] 白金创意设计大赛 第八届 
</li>  

<li> 
• [ 大陆 ] 和谐中国公益设计大赛 第二届 
</li>  

<li> 
• [ 大陆 ] 华人大学生海报创意设计大赛 
</li>  

<li> 
• [ 大陆 ] 华人创新设计大赛
</li>



</ul>
</div>



<br clear="all">

</div>
</div> </div>
                <br clear="all">
					
					  
				
					
				
                </div>
        </div>
			
	<?php endwhile;endif; ?>


<?php get_footer(); ?>
