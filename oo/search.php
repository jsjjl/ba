<?php get_header(); ?>

<body>
    
<?php get_sidebar('top'); ?>

    <div class="contentWrap " >
        <!--左悬浮栏开始-->
        <section class="leftWrap fl">

   
            <!--行业板块选择栏-->
            <div class="navWra category">

                    <ul class="blockIndexClassify category-list fl" style="margin-left:0px;">
                            <li class="tab-program category-hover">搜索结果</li>
                          
                            <div class="indicator"></div>
					</ul>
					<div class="line fl" style="margin-top:0;"></div>
			</div>
			
        

            <!--筛选结果-->

            <!--瀑布流解决方案数据-->
            <div class="content-all content-seld">

			



			<?php $post_query = new WP_Query('showposts=1200'); while ( have_posts() ) : the_post(); ?>


                        <!--item BEGIN-->
                        <a href="<?php the_permalink(); ?>" target="_blank">
                        <div class="content-item ani-form-bottom normalList">
                            <div class="imgItemWra maxH-448 j_indexp">
                            <?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'auto_img' ); } ?>
                            </div>
                            <div class="blockIntroWra">
                                <div class="blockIntro">
                                    <h2 class="blockTitle">
                                        <a href="<?php the_permalink(); ?>" target="_blank" class="normalT pad-t-0"><?php echo mb_strimwidth(get_the_title(), 0, 20, '...'); ?> </a>
                                    </h2>
                                    <p class="blockSummary"><?php echo mb_strimwidth(strip_tags(apply_filters('the_content', $post->post_content)), 0, 140,'...'); ?></p>
                                    <div class="blockBottom">
                                        <a href="" target="_blank" class="blockFrom"><?php the_author(); ?><i class="icon-gf"></i></a>
                                        <i class="bor-right"></i><em class="blockTimeNew handle-time-box" ><?php the_time('Y-n-j'); ?></em>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                        <!--item END-->

						<?php endwhile;?>


            </div>

            <!--loading-->
            <div class="loading-box" >
                <div class="loading-infinite" >
                    <div class="loadimg" style="display: none">
                        <img src="<?php bloginfo('template_directory'); ?>/img/loading.gif" >
                    </div>
                </div>
                <div class="loading-end">已为您显示全部内容</div>
            </div>

        </section>
        <!--左悬浮栏结束-->

        <!--右悬浮栏开始-->
        <section class="rightWrap fr">


        <?php get_sidebar('right-fanan'); ?>

        <?php get_sidebar('right-huodong'); ?>

        <?php get_sidebar('footer'); ?>


        </section>
        <!--右悬浮栏结束-->
    </div>

    <!-- 加载等待 -->
    <div class="load">
        <img src="<?php bloginfo('template_directory'); ?>/img/load.gif">
    </div>
<?php get_footer(); ?>