<?php
/*
Template Name: 忘记密码页面
*/

global $wpdb,$user_ID;

if (!$user_ID) { //判断用户是否登录
//接下来的代码都添加在这里



if($_POST){ //数据提交
    //We shall SQL escape all inputs
    $username = $wpdb->escape($_REQUEST['username']);
    $password = $wpdb->escape($_REQUEST['password']);
    $remember = $wpdb->escape($_REQUEST['rememberme']);
    
    if($remember){
    $remember = "true";
    } else {
    $remember = "false";
    }
    $login_data = array();
    $login_data['user_login'] = $username;
    $login_data['user_password'] = $password;
    $login_data['remember'] = $remember;
    $user_verify = wp_signon( $login_data, false );
    //wp_signon 是wordpress自带的函数，通过用户信息来授权用户(登陆)，可记住用户名
    
    if ( is_wp_error($user_verify) ) {
    echo "<span class='error'>用户名或密码错误，请重试!</span>";//不管啥错误都输出这个信息
    exit();
    } else { //登陆成功则跳转到首页(ajax提交所以需要用js来跳转)
    echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
    exit();
    }
    } else {
    
    //这里添加登录表单代码
?>
<!DOCTYPE html>
<html class="">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
        content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover">
    <title>找回密码</title>
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/app.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/css/layui.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/findPassword.css">
</head>

<body class="concolor">
    <div>
        <!-- 导航开始 -->
        <div id="header" class="navBox">

            <div class="navBox">
                <div class="navCont clearfix">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"" target="_blank">
                        <h1 class="leftLogo fl">戴尔</h1>
                    </a>
                    <div class="rightCont fr">

                        <!-- ico列表 -->
                        <ul class="fr">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>"">解决方案</a>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>
        <!-- 导航结束 -->
        <!-- 主要内容  开始 -->
        <div class="reg-box">
                <div class="register">
                    <h2 class="section-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">找回密码</font></font></h2>
                    <div class="input-list  error-hintBox" id="phone">
                        <div class="input-box">
                            <input type="text" placeholder="请输入您的注册手机/邮箱" data-bind="value:unique" id="phoneValue">
                        </div>
                        <div class="error-hint phoneValue-error" data-bind="validationMessage:unique" id="phoneError" style="display: none;"></div>
                        <!--<div class="error-hint" data-bind="visible: isShow" style="display: none">手机号/邮箱未注册-->
                        <!--</div>-->
                    </div>
                    <div class="input-list error-hintBox">
                        <div class="input-box">
                            <input type="text" class="yzm" placeholder="请输入验证码" data-bind="value: checkCode" id="yzm">
                        </div>
                        <span class="noteCode oct-clickable" id="noteCode" data-bind="attr: {class: isRunning()?'noteCode active':'noteCode'}, text: isRunning() ? ('重新发送(' + remainingTime() +'s)')  : '获取验证码', click: sendCheckCode"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">获取验证码</font></font></span>
                        <!-- <div class="error-hint" data-bind="validationMessage:checkCode" style=""><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请输入验证码</font></font></div> -->
                    </div>
                    <!--<p class="intro-copy note-reveser hiden">短信激活码已发送,请您在10分钟内填写。</p>-->
                    <p class="error-hint intro-copy note-reveser yzm-error" data-bind="visible:isRunning()" style="display: none;">短信激活码已发送,请您在10分钟内填写。</p>
                    <div class="input-list error-hintBox">
                        <div class="input-box">
                            <input type="password" placeholder="请输入您的新密码" data-bind="value: password" id="password">
                        </div>
                        <div class="error-hint password-error" data-bind="validationMessage:confirmPassword" style="display: none;"></div>
                    </div>
                    <div class="input-list error-hintBox">
                        <div class="input-box">
                            <input type="password" placeholder="请再次确认您的新密码" data-bind="value: confirmPassword" id="passwordAgin">
                        </div>
                        <div class="error-hint passwordAgin-error" data-bind="validationMessage:confirmPassword" style="display: none;"></div>
                    </div>
                    <div class="btnbox">
                        <div class="button  oct-clickable-submit" id="button" data-bind="click: submit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保存修改</font></font></div>
                    </div>
                </div>
        
            </div>
    </div>



    <!--公共js引用-->
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/img/layui.all.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/jquery-1.11.2.min.js"></script>

    <script>
    $(function(){


        // 验证手机号
        function isPhoneNo(phone) {
                var pattern = /^1[23456789]\d{9}$/;
                return pattern.test(phone);
        }

        //获取验证码接口
        var trybt = true;
            
        $('.oct-clickable').click(function () {

           
            
            if(trybt == false){
                layer.msg('60s之内无法发送验证码');
                return;
            }
            

            $(".phoneValue-error").hide().html("");
            var _this = $(this);
            var unique=$("#phoneValue").val();

            if(unique == ""){
                $(".phoneValue-error").show().html("<span style='color: #f00;'>请输入注册手机/邮箱</span>");
                return;
            }

            // if (isPhoneNo(unique) == false) {
            //     layer.msg('手机号码不正确');
            //     return;
            // }

            $.ajax({
                type: "post",
                url: "https://sms.yunpian.com/v2/sms/tpl_single_send.json",
                data: { apikey:"all",mobile:unique,tpl_id:1,tpl_value:memberSchemaId,unique:unique},
                dataType: "json",
                success: function (data) {
                    if(data.body.result==0){
                        settime(_this);
                        layer.msg('验证码发送成功');
                        $('.yzm-error').show();
                    }else {
                        layer.msg(data.body.desc);
                    }
                },
                error: function () {
                    layer.msg('系统错误请联系管理员');
                }
            })
        });



        //获取短信验证码
        var countdown = 60;
        function settime(val) {
            if (countdown === 0) {
                val.removeAttr("disabled").text("获取验证码");
                countdown = 60;
                return false;
            } else {
                trybt = false;
                val.attr("disabled", true);
                val.text("重新发送(" + countdown + ")");
                countdown--;
            }
            setTimeout(function () {
                settime(val);
            }, 1000);
        }





        // 提交
        $(".oct-clickable-submit").click(function(){
            var isValid = true,
                phoneValue = $("#phoneValue").val(),
                password = $("#password").val(),
                passwordAgin = $("#passwordAgin").val(),
                phonemobile = $("#phonemobile").val(),
                yzm = $("#yzm").val();
            $(".error-hint").html("");
    

        //验证字段
        if (!phoneValue) {
                $(".phoneValue-error").show().html("<span style='color: #f00;'>请输入注册手机/邮箱</span>");
                isValid = false;
        }

        
        if (!password) {
                $(".password-error").show().html("请输入密码");
                isValid = false;
        }

        if (!passwordAgin) {
                $(".passwordAgin-error").show().html("请再次输入密码");
                isValid = false;
        }

        if(passwordAgin != password){
                $(".passwordAgin-error").show().html("密码不一致");
                isValid = false;
        }

        if (!yzm) {
                $(".yzm-error").show().html("<span style='color: #f00;'>请输入验证码</span>");
                isValid = false;
        }




        // 验证成功
        if (isValid) {
            console.log("验证成功");
            
        }
    });

 

      
    });
    </script>
</body>

</html>




<script type="text/javascript"><!--ajax 提交数据-->
$("#submitbtn").click(function() {
<!--请准备一个gif图-->
$(".oct-clickable").val("登录中");
$('#result').html("");
// $('#result').html('<img src="<?php bloginfo('template_url'); ?>/images/loader.gif" class="loader" />').fadeIn();
var input_data = $('#wp_login_form').serialize();
$.ajax({
type: "POST",
url: "<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>",
data: input_data,
success: function(msg){
$(".oct-clickable").val("登录");
$('#result').html(msg);
}
});
return false;

});
</script>



<?php
}
}else { //跳转到首页
    echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
}

?>
