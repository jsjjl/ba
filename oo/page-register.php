<?php
/*
Template Name: 注册页面
*/

global $wpdb,$user_ID;

if (!$user_ID) { //判断用户是否登录
//接下来的代码都添加在这里



if($_POST){ //数据提交
    //We shall SQL escape all inputs
    $username = $wpdb->escape($_REQUEST['username']);
    $password = $wpdb->escape($_REQUEST['password']);
    $remember = $wpdb->escape($_REQUEST['rememberme']);
    
    if($remember){
    $remember = "true";
    } else {
    $remember = "false";
    }
    $login_data = array();
    $login_data['user_login'] = $username;
    $login_data['user_password'] = $password;
    $login_data['remember'] = $remember;
    $user_verify = wp_signon( $login_data, false );
    //wp_signon 是wordpress自带的函数，通过用户信息来授权用户(登陆)，可记住用户名
    
    if ( is_wp_error($user_verify) ) {
    echo "<span class='error'>用户名或密码错误，请重试!</span>";//不管啥错误都输出这个信息
    exit();
    } else { //登陆成功则跳转到首页(ajax提交所以需要用js来跳转)
    echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
    exit();
    }
    } else {
    
    //这里添加登录表单代码
?>
<!DOCTYPE html>
<html class="">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
        content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover">
    <title>解决方案</title>
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/app.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/css/layui.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/register.1.css">
</head>

<body class="concolor">
    <div>
        <!-- 导航开始 -->
        <div id="header" class="navBox">

            <div class="navBox">
                <div class="navCont clearfix">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" target="_blank">
                        <h1 class="leftLogo fl">戴尔</h1>
                    </a>
                    <div class="rightCont fr">
                        <!-- ico列表 -->
                        <ul class="fr">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">解决方案</a>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>
        <!-- 导航结束 -->
        <!-- 主要内容  开始 -->
        <form id="register-form">
        <div class="reg-box">
  
            <div class="register" style="width:800px">
               
                <div class="left">
                    <h2 class="section-title">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">用户注册</font>
                        </font>
                    </h2>

                    <div class="input-list error-hintBox">
                            <i data-bind="visible:required()=='1'">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">*</font>
                                </font>
                            </i>
                            <div class="input-box">
                                <input type="text" id="name" placeholder="姓名">
                            </div>
                            <div class="error-hint name-error"></div>
                    </div>

                
                    <div class="input-list error-hintBox" id="phone">
                        <i data-bind="visible:required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <div class="input-box">
                            <input type="text" class="phone" data-bind="value: value,attr:{placeholder:displayName}"
                                placeholder="手机" maxlength="11" id="phonemobile">
                        </div>
                        <div class="error-hint phone-error" data-bind="validationMessage: value"></div>
                    </div>
                 
                    <div class="input-list note-ma error-hintBox" data-bind="with: $parent.checkCode">
                        <i>
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <div class="input-box">
                            <input type="text" data-bind="value: value" placeholder="验证码" id="yzm">
                        </div>
                        <span class="noteCode oct-clickable"
                            data-bind="attr: {class: isRunning()?'noteCode oct-clickable active':'noteCode oct-clickable'}, click: sendCheckCode, text: isRunning() ? '重新发送(' + remainingTime() +'s)' : '获取验证码',">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">获取验证码</font>
                            </font>
                        </span>
                        <div class="error-hint" data-bind="validationMessage: value" style="display: none;"></div>
                        <p class="error-hint intro-copy note-reveser yzm-error" data-bind="visible:isRunning()" style="display: none;">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">短信激活码已发送，请您在10分钟内填写。</font>
                            </font>
                        </p>
                    </div>
                
                   
                  
                    <div class="input-list error-hintBox" >
                        <i data-bind="visible:required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <div class="input-box">
                            <input type="text" class="" data-bind="value: value,attr:{placeholder:displayName}"
                                placeholder="邮箱" id="email">
                        </div>
                        <div class="error-hint email-error" data-bind="validationMessage: value" style="">
                           
                        </div>
                    </div>
                
                    <div class="input-list error-hintBox">
                        <i>
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <div class="input-box">
                            <input type="password" id="password"
                                data-bind="value: value,visible: !showPassword(),attr:{placeholder:displayName}"
                                placeholder="密码">
                        </div>
                        <div class="error-hint password-error" data-bind="validationMessage: value" style="display: none;"></div>
                    </div>
                    <div class="input-list error-hintBox">
                        <i>
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <div class="input-box">
                            <input type="password" id="passwordAgin" placeholder="请再次确认密码">
                        </div>
                        <div class="error-hint" id="passwordAginErr"></div>
                    </div>
                
                    <div class="input-list error-hintBox">
                        <i data-bind="visible:required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <div class="input-box">
                            <input type="text" data-bind="value: value,attr:{placeholder:displayName}"
                                placeholder="公司名称" id="comp">
                        </div>
                        <div class="error-hint comp-error" data-bind="validationMessage: value" style="display: none;"></div>
                    </div>


                

                    
                    <div class="note-rideo input-list error-hintBox" style="margin-bottom: 25px;">

                            <i style="line-height: 30px;">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">*</font>
                                    </font>
                                </i>


                            <p class="intro-copy" data-bind="text: displayName()">
                                <font style="vertical-align: inherit;">
                                      
                                        
                                        <font style="vertical-align: inherit;">角色</font>
                                </font>
                            </p>
                            <!--ko foreach: nodes -->
                            <div class="rideo-groups">
                                <!--ko foreach: options -->
                                <div class="radio-boxs">
                                    <input type="radio"
                                        data-bind="attr: {id: index, name: group}, value: $data, checked: $parent.selectedOption"
                                        id="ifWish_level_1_option0" name="ifWish_level_1" value="需求方">
                                    <label data-bind="attr: {for: index}" for="ifWish_level_1_option0"><i></i><span
                                            data-bind="text: option">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">需求方</font>
                                            </font>
                                        </span></label>
                                </div>
    
                                <div class="radio-boxs">
                                    <input type="radio"
                                        data-bind="attr: {id: index, name: group}, value: $data, checked: $parent.selectedOption"
                                        id="ifWish_level_1_option1" name="ifWish_level_1" value="供应商">
                                    <label data-bind="attr: {for: index}" for="ifWish_level_1_option1"><i></i><span
                                            data-bind="text: option">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">供应商</font>
                                            </font>
                                        </span></label>
                                </div>
                                <!--/ko -->
                            </div>
                            <!--/ko -->
                            <div class="error-hint jiaose-error" data-bind="validationMessage: value" style="display: none;"></div>
                        </div>
                       
                  
                    <div class="input-list phones error-hintBox">
                        <i data-bind="visible:required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <div class="box">
                           
                            <div class="zuo number">
                                <input type="text" data-bind="value: value,attr:{placeholder:displayName}"
                                    placeholder="座机号" id="zuo">
                            </div>
                            
                            
                        </div>
                       
                     
                        <div class="error-hint zuo-error" id="extErr">
                       
                        </div>
                    </div>
                  
                    <div class="input-list select-list error-hintBox">
                        <i data-bind="visible:$parent.required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <input class="select demon" style="display: none;" readonly="" unselectable="on"
                            onfocus="this.blur()" type="text"
                            data-bind="value:selectedOptions().option,attr:{placeholder:  $index() == 0 ? $parent.displayName():'请选择'}"
                            placeholder="部门">

                  
                        <div class="select demon" id="bumen" data-bind="text: $parent.displayName" style="color: #666;">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">部门</font>
                            </font>
                        </div>

                        <div class="select-tab">
                            <ul data-bind="foreach: options">
                               

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">高层管理人员</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">后勤/设备/公共设施</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">财务</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">人力资源</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">它</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">市场</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">运营</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">采购</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">销售</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">培训</font>
                                    </font>
                                </li>

                                <li
                                    data-bind="click: (function(){$parent.selectOption($data);$root.fun(option)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">其它</font>
                                    </font>
                                </li>
                            </ul>
                        </div>
                      
                        <div class="error-hint bumen-error" data-bind="visible:$index() == 0,validationMessage: $parent.value"
                            style="display: none;"></div>
                      

                    </div>
                
                    <div class="input-list select-list error-hintBox">
                        <i data-bind="visible:$parent.required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <input class="select demon" style="display: none;" readonly="" unselectable="on"
                            onfocus="this.blur()" type="text"
                            data-bind="value:selectedOptions().option,attr:{placeholder:  $index() == 0 ? $parent.displayName():'请选择'}"
                            placeholder="省份">

                        <div class="select demon" id="shenfen" data-bind="text: ($index() ? '城市' : '省份' )" style="color: #666;">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">省份</font>
                            </font>
                        </div>

                      

                        <div class="select-tab">
                            <ul data-bind="foreach: options">
                               

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">北京</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">天津</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">河北</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">山西</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">内蒙古</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">辽宁</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">吉林</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">黑龙江</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">上海</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">江苏</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">浙江</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">安徽</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">福建</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">江西</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">山东</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">河南</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">湖北</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">湖南</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">广东</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">广西</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">海南</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">重庆</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">四川</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">贵州</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">云南</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">西藏</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">陕西</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">甘肃</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">青海</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">宁夏</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">新疆</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">台湾</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">香港</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">澳门</font>
                                    </font>
                                </li>
                            </ul>
                        </div>

                    
                        <div class="error-hint shenfen-error" data-bind="visible:$index() == 0,validationMessage: $parent.value" style="display: none;"></div>
                 

                    </div>
                 
                    <div class="input-list error-hintBox">
                        <i data-bind="visible:required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <div class="input-box">
                            <input type="text" data-bind="value: value,attr:{placeholder:displayName}"
                                placeholder="详细地址" id="address">
                        </div>
                        <div class="error-hint address-error" data-bind="validationMessage: value" style="display: none;"></div>
                    </div>
                  
                    <div class="input-list select-list error-hintBox">
                        <i data-bind="visible:$parent.required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <input class="select demon" style="display: none;" readonly="" unselectable="on"
                            onfocus="this.blur()" type="text"
                            data-bind="value:selectedOptions().option,attr:{placeholder:  $index() == 0 ? $parent.displayName():'请选择'}"
                            placeholder="行业">

                       
                        <div class="select demon" id="hanye" data-bind="text: $parent.displayName" style="color: #666;">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">行业</font>
                            </font>
                        </div>

                        <div class="select-tab">
                            <ul data-bind="foreach: options">
                             

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">互联网相关企业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">IT服务业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">物流运输业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">其他服务行业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">高科技制造业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">普通制造业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">金融行业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">批发零售业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">电信</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">媒体，娱乐</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">能源</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">政府，事业单位</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">医疗行业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">教育行业</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">房地产</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">其他</font>
                                    </font>
                                </li>
                            </ul>
                        </div>
                       
                        <div class="error-hint hanye-error" data-bind="visible:$index() == 0,validationMessage: $parent.value"
                            style="display: none;"></div>
                     

                    </div>
                  
                    <div class="input-list select-list error-hintBox">
                        <i data-bind="visible:$parent.required()=='1'">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">*</font>
                            </font>
                        </i>
                        <input class="select demon" style="display: none;" readonly="" unselectable="on"
                            onfocus="this.blur()" type="text"
                            data-bind="value:selectedOptions().option,attr:{placeholder:  $index() == 0 ? $parent.displayName():'请选择'}"
                            placeholder="公司人数">

                       
                        <div class="select demon" id="renshu" data-bind="text: $parent.displayName" style="color: #666;">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">公司人数</font>
                            </font>
                        </div>

                        <div class="select-tab">
                            <ul data-bind="foreach: options">
                              

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">1-25</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">26-99</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">100-499</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">500-999</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">1000-4999</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">5000-9999</font>
                                    </font>
                                </li>

                                <li data-bind="click: (function(){$parent.selectOption($data)}), text: option">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">10000+</font>
                                    </font>
                                </li>
                            </ul>
                        </div>
                      
                        <div class="error-hint renshu-error" data-bind="visible:$index() == 0,validationMessage: $parent.value"
                            style="display: none;"></div>
                      

                    </div>
                   


                    <div class="statementBox input-list error-hintBox">
                       
                        <div class="statement" style="display: flex;">
                            <div class="checked-boxs">
                                <input type="checkbox"
                                    data-bind="attr: {id: index, name: group}, value: $data, checked: $parent.selectedOption"
                                    id="privacynotice_level_1_option0" name="privacynotice_level_1"
                                    value="[object Object]">
                                <label data-bind="attr: {for: index}" for="privacynotice_level_1_option0">
                                    <i></i>
                                    <p class="intro-copy" data-bind="text: option">
                                        <font style="vertical-align: inherit;">
                                            <font style="vertical-align: inherit;">我同意：戴尔将根据隐私政策收集您的个人信息，并通过上述信息与您联系。</font>
                                        </font>
                                    </p>
                                </label>
                            </div>

                        </div>
              
                        <div class="error-hint" data-bind="validationMessage: value" style="display: none;"></div>
                    </div>
                   

                    <div class="btnbox">
                        <div class="button oct-clickable-submit">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">提交</font>
                            </font>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        </form>
        <!-- 主要内容  结束 -->
    </div>
    <!--公共js引用-->
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/img/layui.all.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/img/jquery-1.11.2.min.js"></script>
    <script>
    $(function () {

        //显示下拉
        $(document).on("click", ".select-list", function (e) {
                var ev = e || window.event;
                if (ev.stopPropagation) {
                ev.stopPropagation();
                }
                else if (window.event) {
                window.event.cancelBubble = true;//兼容IE
                }

                var _this = $(this).find(".select-tab");
                $(".select-tab").not(_this).removeClass("on").siblings(".select").addClass("demon");
                $(this).find(".select-tab").toggleClass("on").siblings(".select").toggleClass("demon");
        });


        $(document).on('click', function () {
                $(".select-tab.on").removeClass("on");
                $(".select").addClass("demon");
        });

        //隐藏下拉菜单
        $('.select-tab ul li').click(function () {
            $(this).parent().parent().prev().html($(this).html());
            $(".select-tab").not(".select-tab").removeClass("on").siblings(".select").addClass("demon");
            $(this).find(".select-tab").toggleClass("on").siblings(".select").toggleClass("demon");
        });

        // 验证手机号
        function isPhoneNo(phone) {
                var pattern = /^1[23456789]\d{9}$/;
                return pattern.test(phone);
        }

        //获取验证码接口
        var trybt = true;
            
        $('.oct-clickable').click(function () {
            
            if(trybt == false){
                layer.msg('60s之内无法发送验证码');
                return;
            }
            

            $("#phone .error-hint").hide().html("");
            var _this = $(this);
            var unique=$(".phone").val();

            if(unique == ""){
                $("#phone .error-hint").show().html("请输入手机号");
                return;
            }

            if (isPhoneNo(unique) == false) {
                layer.msg('手机号码不正确');
                return;
            }


            $.ajax({
                type: "post",
                url: "<?php echo get_template_directory_uri(); ?>/ajax/messager.php",
                data: { 
                    mobile:unique,
                },
                dataType: "json",
                success: function (data) {
                    if(data.body.result==0){
                        settime(_this);
                        layer.msg('验证码发送成功');
                        $('.note-reveser').show();
                    }else {
                        layer.msg(data.body.desc);
                    }
                },
                error: function () {
                    layer.msg('系统错误请联系管理员');
                }
            })
        });





        //获取短信验证码
        var countdown = 60;
        function settime(val) {
            if (countdown === 0) {
                val.removeAttr("disabled").text("获取验证码");
                countdown = 60;
                return false;
            } else {
                trybt = false;
                val.attr("disabled", true);
                val.text("重新发送(" + countdown + ")");
                countdown--;
            }
            setTimeout(function () {
                settime(val);
            }, 1000);
        }









   $(".oct-clickable-submit").click(function(){
        var isValid = true,
            name = $("#name").val(),
            phonemobile = $("#phonemobile").val(),
            email = $("#email").val(),
            yzm = $("#yzm").val(),
            password = $("#password").val(),
            passwordAgin = $("#passwordAgin").val(),
            jiaose = $('input:radio:checked').val(),
            zuo = $('#zuo').val(),
            comp = $('#comp').val(),
            address = $('#address').val(),
            renshu = $('#renshu font font').html() ,
            hanye = $('#hanye font font').html() ,
            shenfen = $('#shenfen font font').html() ,
            bumen = $('#bumen font font').html() 
            ;
        $(".error-hint").html("");
    
      //验证字段
      if (!name) {
            $(".name-error").show().html("请输入姓名");
            isValid = false;
      }

      if (!phonemobile) {
            $(".phone-error").show().html("请输入手机号");
            isValid = false;
      }

      if (!email) {
            $(".email-error").show().html("请输入邮箱");
            isValid = false;
      }

      if (!yzm) {
            $(".yzm-error").show().html("<span style='color: #f00;'>请输入验证码</span>");
            isValid = false;
      }

      

      if (!password) {
            $(".password-error").show().html("请输入密码");
            isValid = false;
      }

      if (!passwordAgin) {
            $("#passwordAginErr").show().html("请再次输入密码");
            isValid = false;
      }

      if(passwordAgin != password){
            $("#passwordAginErr").show().html("密码不一致");
            isValid = false;
      }

      if (!comp) {
            $(".comp-error").show().html("请输入公司名称");
            isValid = false;
      }

      if($("input[type='checkbox']").is(':checked') != true){
            layer.msg("请同意隐私政策！");
            isValid = false;
      }

      if(jiaose == undefined){
            $(".jiaose-error").show().html("请选择角色");
            isValid = false;
      }

      if(!zuo){
            $(".zuo-error").show().html("请输入座机号");
            isValid = false;
      }

      if(!address){
            $(".address-error").show().html("请输入详细地址");
            isValid = false;
      }

      if(renshu== "公司人数"){
            $(".renshu-error").show().html("请选择角色");
            isValid = false;
      }

      if(hanye == "行业"){
            $(".hanye-error").show().html("行业");
            isValid = false;
      }

      if(shenfen == "省份"){
            $(".shenfen-error").show().html("请选择省份");
            isValid = false;
      }

      if(bumen == "部门"){
            $(".bumen-error").show().html("请选择部门");
            isValid = false;
      }

        if (!isValid) {
            return;
        }

        const data = {
            user_name: name,
            user_mobile: phonemobile,
            user_email: email,
            user_mobile_code: yzm,
            user_pass: password,
            user_pass_2: passwordAgin,
            jiaose,
            zuo,
            comp,
            address,
            renshu,
            hanye,
            shenfen,
            bumen,
        };
        $.post('<?php echo get_template_directory_uri(); ?>/ajax/register.php', data).then(data => {
            layer.msg('成功');
            window.location.href = '<?php bloginfo('url'); ?>/login';
        }, (e) => {
            layer.msg(e);
        });
    });

 
    //   顶部效果
        $(window).on('scroll', function (e) {
            var bannerH = $('.bannerBox').height();
            var scrollH = $(window).scrollTop();
            if (scrollH > bannerH) {
                $('.navBox').addClass('on');
            } else {
                $('.navBox').removeClass('on');
            }
            $(".select-tab.on").removeClass("on");
            $(".select").addClass("demon");
        })


    })
</script>
</body>

</html>

<script type="text/javascript"><!--ajax 提交数据-->
$("#submitbtn").click(function() {
<!--请准备一个gif图-->
$(".oct-clickable").val("登录中");
$('#result').html("");
// $('#result').html('<img src="<?php bloginfo('template_url'); ?>/images/loader.gif" class="loader" />').fadeIn();
var input_data = $('#wp_login_form').serialize();
$.ajax({
type: "POST",
url: "<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>",
data: input_data,
success: function(msg){
$(".oct-clickable").val("登录");
$('#result').html(msg);
}
});
return false;

});
</script>



<?php
}
}else { //跳转到首页
    echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
}

?>
