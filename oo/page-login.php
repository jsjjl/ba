<?php
/*
Template Name: 登陆页面
*/

global $wpdb,$user_ID;

if (!$user_ID) { //判断用户是否登录
//接下来的代码都添加在这里



if($_POST){ //数据提交
    //We shall SQL escape all inputs
    $username = $wpdb->escape($_REQUEST['username']);
    $password = $wpdb->escape($_REQUEST['password']);
    $remember = $wpdb->escape($_REQUEST['rememberme']);
    
    if($remember){
    $remember = "true";
    } else {
    $remember = "false";
    }
    $login_data = array();
    $login_data['user_login'] = $username;
    $login_data['user_password'] = $password;
    $login_data['remember'] = $remember;
    $user_verify = wp_signon( $login_data, false );
    //wp_signon 是wordpress自带的函数，通过用户信息来授权用户(登陆)，可记住用户名
    
    if ( is_wp_error($user_verify) ) {
    echo "<span class='error'>用户名或密码错误，请重试!</span>";//不管啥错误都输出这个信息
    exit();
    } else { //登陆成功则跳转到首页(ajax提交所以需要用js来跳转)
    echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
    exit();
    }
    } else {
    
    //这里添加登录表单代码
?>
<!--<script src="http://code.jquery.com/jquery-1.4.4.js"></script>--><!--如果你发现没有ajax提交，请检查是否有引入jquery.js文件-->
<!DOCTYPE html>
<html class="">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport"
        content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover">
    <title>解决方案</title>
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/app.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/img/login.1.css">
    <script src="<?php bloginfo('template_directory'); ?>/img/jquery-1.11.2.min.js"></script>
</head>

<body class="concolor">
    <div>
        <!-- 导航开始 -->
        <div id="header" class="navBox">

            <div class="navBox">
                <div class="navCont clearfix">
                    <a href="" target="_blank">
                        <h1 class="leftLogo fl">戴尔</h1>
                    </a>
                    <div class="rightCont fr">

                        <!-- ico列表 -->
                        <ul class="fr">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>">解决方案</a>
                            </li>
                        </ul>

                    </div>
                </div>

            </div>
        </div>
        <!-- 导航结束 -->
        <!-- 主要内容  开始 -->
        <div class="reg-box">

            <div class="register">
                <h2 class="section-title">用户登录</h2>



                <form id="wp_login_form" action="" method="post">
                    <div class="input-list error-hintBox">
                        <div class="input-box">
                            <input id="unique" data-bind="value: identity" type="text" placeholder="手机/邮箱"  name="username" class="text">
                        </div>
                        <div data-bind="validationMessage: identity" class="error-hint" style="display: none;"></div>
                    </div>
                    <div class="input-list error-hintBox">
                        <div class="input-box">
                            <input id="pass" data-bind="value: password" type="password"
                                placeholder="登录密码"   name="password"  class="text">
                        </div>
                        <div data-bind="validationMessage: password" class="error-hint" id="result"></div>
                        <div data-bind="text: loginErrorMessage" class="error-hint"></div>
                    </div>
                    <div class="btnbox">
                        <!-- <input name="rememberme" type="checkbox" value="forever" />记住我</label> -->
                    
                        <input type="submit" id="submitbtn" name="submit" value="登录" class="button oct-clickable"/>

                        <!-- <div data-bind="click: submit" >登录</div> -->
                    </div>

                </form>




                <div class="login intro-copy">没有账号？<a href="<?php echo esc_url( home_url( '/' ) ); ?>register">立即注册</a><a class="forget-password"
                        href="<?php echo esc_url( home_url( '/' ) ); ?>findpassword">忘记密码</a></div>

            </div>

        </div>



    </div>

</body>

</html>

<script type="text/javascript"><!--ajax 提交数据-->
$("#submitbtn").click(function() {
<!--请准备一个gif图-->
$(".oct-clickable").val("登录中");
$('#result').html("");
// $('#result').html('<img src="<?php bloginfo('template_url'); ?>/images/loader.gif" class="loader" />').fadeIn();
var input_data = $('#wp_login_form').serialize();
$.ajax({
type: "POST",
url: "<?php echo "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>",
data: input_data,
success: function(msg){
$(".oct-clickable").val("登录");
$('#result').html(msg);
}
});
return false;

});
</script>



<?php
}
}else { //跳转到首页
    echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
}

?>
